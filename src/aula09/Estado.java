package aula09;

public class Estado extends Regi�o {

	private String nome;
	private int populacao;
	private Localidade capital;
	public Estado(String nome, int populacao, Localidade capital) {
		if (capital.getTipo() == TipoLocalidade.Cidade){
			this.nome = nome;
			this.populacao = populacao;
			this.capital = capital;
		}
	}
	
	public Estado() {
		this.nome = "Indefinifo";
		this.populacao = 0;
		this.capital = new Localidade();
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getPopulacao() {
		return populacao;
	}

	public void setPopulacao(int populacao) {
		this.populacao = populacao;
	}

	public Localidade getCapital() {
		return capital;
	}

	public void setCapital(Localidade capital) {
		this.capital = capital;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((capital == null) ? 0 : capital.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + populacao;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estado other = (Estado) obj;
		if (capital == null) {
			if (other.capital != null)
				return false;
		} else if (!capital.equals(other.capital))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (populacao != other.populacao)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Estado: " + nome + ", Popula��o=" + populacao + ", Capital=" + capital;
	}
	
	
	
	
}
