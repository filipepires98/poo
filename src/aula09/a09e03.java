package aula09;
import java.util.*;

public class a09e03 {

	public static void main(String[] args) {
		Localidade cid1 = new Localidade("Szohod", 31212, TipoLocalidade.Cidade);
		Localidade cid2 = new Localidade("Wadesdah", 23423, TipoLocalidade.Cidade);
		Localidade cid3 = new Localidade("BedRock", 23423, TipoLocalidade.Vila);
		Estado est1 = new Estado("North Borduria", 223133, cid1);
		Estado est2 = new Estado("South Borduria", 84321, cid2);
		Pa�s p1 = new Pa�s("Borduria", est1.getCapital());
		Pa�s p2 = new Pa�s("Khemed", cid2);
		Pa�s p3 = new Pa�s("Aurelia");
		Pa�s p4 = new Pa�s("Atlantis");
		p1.addRegiao(est1);
		p1.addRegiao(est2);
		p2.addRegiao(new Prov�ncia("Afrinia", 232475, "Aluko Pono"));
		p2.addRegiao(new Prov�ncia("Eriador", 100000, "Dumpgase Liru"));
		p2.addRegiao(new Prov�ncia("Laurania", 30000, "Mukabamba Dabba"));
		List<Pa�s> org = new ArrayList<Pa�s>();
		org.add(p1);
		org.add(p2);
		org.add(p3);
		org.add(p4);
		System.out.println("----Iterar sobre o conjunto");
		Iterator<Pa�s> itr = org.iterator();
		while (itr.hasNext())
			System.out.println(itr.next());
			System.out.println("-------Iterar sobre o conjunto - For each (java 8)");
			for (Pa�s pais: org) System.out.println(pais);
		// ToDo:
		// adicionar, remover, ordenar, garantir elementos �nicos
		}

}

