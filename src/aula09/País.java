package aula09;
import java.util.*;

public class Pa�s {

	private String nome;
	private Localidade capital;
	private Set<Regi�o> regioes=new HashSet<Regi�o>();
	
	public Pa�s() {
		this.nome = "Indefinido";
		this.capital = new Localidade();
	}
	
	public Pa�s(String nome) {
		this.nome = nome;
		this.capital = new Localidade();
	}
	
	public Pa�s(String nome, Localidade capital) {
		this.nome = nome;
		this.capital = capital;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Localidade getCapital() {
		return capital;
	}
	public void setCapital(Localidade capital) {
		this.capital = capital;
	}
	
	public void addRegiao(Regi�o r){
		regioes.add(r);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((capital == null) ? 0 : capital.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((regioes == null) ? 0 : regioes.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pa�s other = (Pa�s) obj;
		if (capital == null) {
			if (other.capital != null)
				return false;
		} else if (!capital.equals(other.capital))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (regioes == null) {
			if (other.regioes != null)
				return false;
		} else if (!regioes.equals(other.regioes))
			return false;
		return true;
	}

	@Override
	public String toString() {
		long n;
		String control;
		if(capital.getNome().equals("Indefinido")){
			control="*Indefinido*";
			n=0;
		}
		else{
			control =  capital.toString();
			n=capital.getPopulacao();
			for (Regi�o a : regioes){
				n+=a.getPopulacao();
			}
		}
		return "Pa�s:" + nome + ", Popula��o: " + n + " (Capital: " + control+")";
	}
	
	
	
}
