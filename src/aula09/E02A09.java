package aula09;
import java.util.*;

public class E02A09 {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        String[] funcionarios = {"Pedro Barbosa","André Ferreira","Gustavo Pires","Rita Silva","Mariana Yi"};
        String[] brinquedos = {"legos","action-man","barbie-house","spring-gun","monopoly","poker","nerf-gun","firecrackers","star-wars-figures"};
        int[] numbrinq = {0,0,0,0,0};

        // a)     B&B
        System.out.println("Funcionários da empresa B&B: ");
        ArrayList<String> listaFuncionarios = new ArrayList<>();                 // criacao de arraylist com todos os funcionarios da empresa
        for (String f: funcionarios) {
            listaFuncionarios.add(f);
        }
        for (String a: listaFuncionarios) {
            System.out.println(a);
        }

        // b)     hashmap(funcionario,brinquedo)
        System.out.println("\nBrinquedos oferecidos no ano de 2017: ");
        Random r = new Random();
        int maxf = funcionarios.length;
        int maxb = brinquedos.length;
        Map<String,Set<String>> mapamensal = new HashMap<String,Set<String>>();              // criacao de mapa anual

        for (int i = 0; i<12; i++) {                                                         // oferta de brinquedo a cada mes
            int randomf = r.nextInt(maxf);     // random index funcionarios
            int randomb = r.nextInt(maxb);     // random index brinquedos
            Set<String> brinq;
            if (!(mapamensal.containsKey(funcionarios[randomf]))) {
                brinq = new HashSet<>();
            }  else {
                brinq = mapamensal.get(funcionarios[randomf]);
            }
            brinq.add(brinquedos[randomb]);
            mapamensal.put(funcionarios[randomf],brinq);
        }
        List<String> keys = new ArrayList<String>(mapamensal.keySet());
        for (int index = 0; index < keys.size(); index++) {
            System.out.println("("+keys.get(index) + "," + mapamensal.get(keys.get(index))+")");
            numbrinq[index] = mapamensal.get(keys.get(index)).size();                                     // for ex d)
            System.out.println(numbrinq[index]);
        }

        // c) set (nome produtos)
        System.out.println("\nColeção Nova: ");
        Set<String> collection = new HashSet<>();                           // coleçao de brinquedos com nomes de funcionarios
        List<String> brinqselected = new ArrayList<>();                     // arraylist de brinquedos seleccionados
        int i = 1;
        while (collection.size() < funcionarios.length) {
            int randomcf = r.nextInt(maxf);                                 // funcionario escolhido
            int randomcb = r.nextInt(maxb);                                 // brinquedo cujo nome será mudado

            String[] nomes = funcionarios[randomcf].split(" ");
            int size1 = collection.size();
            collection.add(nomes[0]);                                        // seleccao do primeiro nome do funcionario
            int size2 = collection.size();
            if (size1 != size2) {
                if (!brinqselected.contains(brinquedos[randomcb])) {         // verificar se brinquedo ja pertence a colecao com o nome de algum funcionario
                    System.out.println(i + " - " +  nomes[0] + " (" + brinquedos[randomcb] + ")");
                    brinqselected.add(brinquedos[randomcb]);
                } else {
                    while (brinqselected.contains(brinquedos[randomcb])) {            // encontrar brinquedo disponivel caso necessario
                        randomcb = r.nextInt(maxb);
                    }
                    System.out.println(i + " - " +  nomes[0] + " (" + brinquedos[randomcb] + ")");
                    brinqselected.add(brinquedos[randomcb]);
                }
                i++;
            }
        }

        // d) funcionarios mais populares   - suponhamos que quem recebe mais presentes ao longo do mes sao os melhores funcionarios, apesar da distribuicao ser random
        System.out.println("\n3 Primeiros Funcionários Seleccionados: ");
        int one = 0;
        int ione = 0;
        int two = 0;
        int itwo = 0;
        int three = 0;
        int ithree = 0;
        for (int index = 0; index < funcionarios.length; index++) {
            if (numbrinq[index] > one) {
                one = numbrinq[index];
                ione = index;
                //System.out.println(one + "," + two + "," + three);
            } else if (numbrinq[index] <= one && numbrinq[index] > two) {
                two = numbrinq[index];
                itwo = index;
                //System.out.println(one + "," + two + "," + three);
            } else if (numbrinq[index] <= two && numbrinq[index] > three) {
                three = numbrinq[index];
                ithree = index;
                //System.out.println(one + "," + two + "," + three);
            }
        }
        System.out.println("1º- " + funcionarios[ione]);
        System.out.println("2º- " + funcionarios[itwo]);
        System.out.println("3º- " + funcionarios[ithree]);

    }
}
