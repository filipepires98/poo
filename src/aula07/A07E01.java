package aula07;
import java.util.*;

public class A07E01 {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {

        ArrayList<Object> a1 = new ArrayList<Object>();
        ArrayList<Object> fal = new ArrayList<Object>();
        boolean control = true;

        // criei 3 veiculos de teste
        Veiculo a = new Automovel("00-AA-01", "preto", 1970, 4, 80, 300, 5, true);
        Veiculo v = new Velocipede("Hell Yeah", "preto", 2000, 2, 0, 60, 1, false);
        Veiculo m = new Motociclo("00-MM-01", "preto", 1990, 2, 80, 300, 2, true);
        a1.add(a);
        a1.add(v);
        a1.add(m);

        while (control) {
            System.out.println("What do you whish to do? (1= create Veiculo, 2= print sorted list of Veiculos, 3= exit) ");
            switch(sc.nextInt()) {
                case 1:
                    int tipo = 0;
                    while (tipo !=1 && tipo!=2 && tipo!=3) {
                        System.out.print("What kind of Veiculo do you wish to create? (1 = Automovel, 2 = Motociclo, 3 = Velocipede) ");
                        tipo = sc.nextInt();
                    }
                    switch (tipo) {
                        case 1:
                            Veiculo auto = new Automovel();
                            a1.add(auto);
                            break;
                        case 2:
                            Veiculo moto = new Motociclo();
                            a1.add(moto);
                            break;
                        case 3:
                            Veiculo velo = new Velocipede();
                            a1.add(velo);
                            break;
                    }
                    break;
                case 2:
                    int tamanho = a1.size();
                    while (fal.size() != tamanho) {
                        int max = 0;
                        for (Object x: a1) {          // encontrar ano mais recente
                            if ((((Veiculo)x).getAno() > max)) {
                                    max = ((Veiculo)x).getAno();
                                }
                            }
                        for (Object x: a1) {          // adicionar a lista ordenada veiculo de ano mais recente
                            if (((Veiculo)x).getAno() == max){
                                fal.add(x);
                            }
                        }
                        for (Object x: fal) {         // imprimir e remover de lista provisoria veiculo ja adicionado
                            if (a1.contains(x)) {
                                System.out.println(((Veiculo)x).getAno() + " - " + x.toString());
                                a1.remove(x);
                            }
                        }
                    } // end of while
                    break;
                case 3:
                    control = false;
                    break;
                default:
                    System.out.println("Invalid Input!");
                    break;
            } // end of switch
        } // end of while (menu)
        System.out.println("Have a nice day!");
    }
}
