package aula07;
import java.util.*;

public class Pessoa implements IDemo123 {
    private Scanner sc = new Scanner(System.in);
    private String nome;
    private int cc;
    private String data;

    public int getCc() {return cc;}
    public String getNome() {return nome;}
    public String getData() {return data;}

    public void setCc(int cc) {this.cc = cc;}
    public void setNome(String nome) {this.nome = nome;}
    public void setData(String data) {this.data = data;}

    public Pessoa() {                                          // pre-definido
        this.nome = "Filipe Pires";
        this.cc = 13831291;
        this.data = "25-06-1998";
    }
    public Pessoa(String nome, int cc, String data) {
        this.nome = nome;
        this.cc = cc;
        this.data = data;
    }

    public boolean FilmeGosto(){
        System.out.println("Gosta da saga Star Wars? (s/n) ");
        if (sc.next().equals("s")) {
            return true;
        }
        return false;
    }
    public boolean SerieGosto(){
        System.out.println("Gosta da serie Mr.Robot? (s/n) ");
        if (sc.next().equals("s")) {
            return true;
        }
        return false;
    }
    public boolean FrequentaGinasio() {
        String[] Data = data.split("-");
        if (Integer.parseInt(Data[2]) > 1998 || Integer.parseInt(Data[2]) < 1900) {
            return false;
        }
        System.out.println("Frequenta ginásio? (s/n) ");
        if (!(sc.next().equals("s"))) {
            return false;
        }
        return true;
    }
    public boolean FrequentaUA() {
        String[] Data = data.split("-");
        if (Integer.parseInt(Data[2]) > 1998 || Integer.parseInt(Data[2]) < 1900) {
            return false;
        }
        System.out.println("Frequenta a UA? (s/n) ");
        if (!(sc.next().equals("s"))) {
            return false;
        }
        return true;
    }
    public int NumIrmaos() {
        int num = -1;
        while (num < 0 || num > 30) {
            System.out.println("Número de irmãos: ");
            num = sc.nextInt();
        }
        return num;
    }
    public int Mensalidade() {
        int mensalidade = 0;
        if (FrequentaUA()) {
            if (FrequentaGinasio()) {
                mensalidade = 150+30;
            } else {
                mensalidade = 150;
            }
        } else {
            if (FrequentaGinasio()) {
                mensalidade = 50+30;
            } else {
                mensalidade = 50;
            }
        }
        return mensalidade;
    }
    public int HorasLivres() {
        int hl = -1;
        while (hl < 0 || hl > 24*7) {
            System.out.println("Horas livres semanais: ");
            hl = sc.nextInt();
        }
        return hl;
    }

    @Override
    public String toString() {
        return "Pessoa{" + "nome= '" + nome + '\'' + ", cc= " + cc + ", data de nascimento= '" + data + '\'' + '}';
    }
}
