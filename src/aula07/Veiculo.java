package aula07;
import java.util.*;

public abstract class Veiculo {
    public Scanner sc = new Scanner(System.in);
    private String matricula, cor;
    private int ano, rodas, cilindrada, velmax, lotmax;
    private boolean ae;

    public Veiculo(String matricula, String cor, int ano, int rodas, int cilindrada, int velmax, int lotmax, boolean ae) {
        this.matricula = matricula;
        this.cor = cor;
        this.ano = ano;
        this.rodas = rodas;
        this.cilindrada = cilindrada;
        this.velmax = velmax;
        this.lotmax = lotmax;
        this.ae = ae;
    }

    public Veiculo() {
        System.out.print("Matrícula (se for um velocípede pode escrever o que quiser, o input não será validado): ");
        matricula = sc.next();
        System.out.print("Cor: ");
        cor = sc.next();
        System.out.print("Ano: ");
        ano = sc.nextInt();
        System.out.print("Número de Rodas: ");
        rodas = sc.nextInt();
        System.out.print("Cilindrada (se for um velocípede escreva aqui o valor 0): ");
        cilindrada = sc.nextInt();
        System.out.print("Velocidade Máxima (km/h): ");
        velmax = sc.nextInt();
        System.out.print("Lotação Máxima: ");
        lotmax = sc.nextInt();
        System.out.print("Pode andar em Auto-estradas? (s/n) ");
        String resposta = sc.next();
        if (resposta.equals("s") || resposta.equals("S")) {
            ae = true;
        } else {
            ae = false;
        }
    }

    public String getMatricula() {return matricula;}
    public void setMatricula(String matricula) {this.matricula = matricula;}
    public String getCor() {return cor;}
    public void setCor(String cor) {this.cor = cor;}
    public int getAno() {return ano;}
    public void setAno(int ano) {this.ano = ano;}
    public int getRodas() {return rodas;}
    public void setRodas(int rodas) {this.rodas = rodas;}
    public int getCilindrada() {return cilindrada;}
    public void setCilindrada(int cilindrada) {this.cilindrada = cilindrada;}
    public int getVelmax() {return velmax;}
    public void setVelmax(int velmax) {this.velmax = velmax;}
    public int getLotmax() {return lotmax;}
    public void setLotmax(int lotmax) {this.lotmax = lotmax;}
    public boolean isAe() {return ae;}
    public void setAe(boolean ae) {this.ae = ae;}

    @Override
    public String toString() {
        return "matricula= '" + matricula + '\'' + ", cor= '" + cor + '\'' + ", ano= " + ano + ", rodas= " + rodas + ", cilindrada= " + cilindrada + "cm3, velmax= " + velmax + "km/h, lotmax= " + lotmax + ", acesso a autoestradas= " + ae;
    }
}
