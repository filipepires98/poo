package aula07;

public class Automovel extends Veiculo implements Explosao, Eletrico {                  // considerei o automovel como qualquer veiculo a motor que não um motociclo ou ciclomotor
    private static int tipo;
    private double emissao;
    private String autonomia;
    public Automovel(String matricula, String cor, int ano, int rodas, int cilindrada, int velmax, int lotmax, boolean ae) {
        super(matricula, cor, ano, rodas, cilindrada, velmax, lotmax, ae);
        while (!(matricula.contains("-")) || ano<1950 || ano>2017 || rodas >= 8 || rodas <= 3 || lotmax < 1) {
            if (!(matricula.contains("-"))) {
                System.out.print("Matrícula Inválida, defina-a novamente: ");
                super.setMatricula(sc.next());
            }
            if (ano<1950 || ano>2017) {
                System.out.print("\nAno Inválido, defina-o novamente: ");
                super.setAno(sc.nextInt());
            }
            if (rodas >= 8 || rodas <= 3) {
                System.out.print("\nNúmero Inválido de rodas, defina-o novamente: ");
                super.setRodas(sc.nextInt());
            }
            if (lotmax < 1) {
                System.out.print("\nNúmero Inválido de lotação máxima, defina-a novamente: ");
                super.setLotmax(sc.nextInt());
            }
        }
        if (cilindrada <= 50) {
            if (ae == true) {
                System.out.print("\nCilindrada inferior à permitida para a utilização das autoestradas nacionais, permissão redefinida automaticamente.");
                super.setAe(false);
            }
        }
        while (tipo !=1 && tipo!=2 && tipo!=3) {
            System.out.print("O seu automóvel é de que tipo? (1 = a combustão, 2 = elétrico, 3 = híbrido) ");
            tipo = sc.nextInt();
        }
    }
    public Automovel() {
        while (!((super.getMatricula()).contains("-")) || super.getAno()<1950 || super.getAno()>2017 || super.getRodas() >= 8 || super.getRodas() <= 3 || super.getLotmax() < 1) {
            if (!((super.getMatricula()).contains("-"))) {
                System.out.print("Matrícula Inválida, defina-a novamente: ");
                super.setMatricula(sc.next());
            }
            if (super.getAno()<1950 || super.getAno()>2017) {
                System.out.print("\nAno Inválido, defina-o novamente: ");
                super.setAno(sc.nextInt());
            }
            if (super.getRodas() >= 8 || super.getRodas() <= 3) {
                System.out.print("\nNúmero Inválido de rodas, defina-o novamente: ");
                super.setRodas(sc.nextInt());
            }
            if (super.getLotmax() < 1) {
                System.out.print("\nNúmero Inválido de lotação máxima, defina-a novamente: ");
                super.setLotmax(sc.nextInt());
            }
        }
        if (super.getCilindrada() <= 50) {
            if (super.isAe() == true) {
                System.out.print("\nCilindrada inferior à permitida para a utilização das autoestradas nacionais, permissão redefinida automaticamente.");
                super.setAe(false);
            }
        }
        while (tipo !=1 && tipo!=2 && tipo!=3) {
            System.out.print("O seu automóvel é de que tipo? (1 = a combustão, 2 = elétrico, 3 = híbrido) ");
            tipo = sc.nextInt();
        }
    }

    public double getEmissaoCO2() {
        switch(tipo) {                  // vou utilizar o valor da cilindrada para calcular a emissao de co2 msm sabendo que esse nao e o processo real...
            case 1:
                emissao = super.getCilindrada()*3;     // equacao errada, apenas pretendo obter um valor para a emissao
                break;
            case 2:
                emissao = 0;
            case 3:
                emissao = super.getCilindrada()*2;
        }
        return emissao;
    }
    public String getAutonomia() {
        switch(tipo) {                  // vou utilizar o valor da cilindrada para calcular a emissao de co2 msm sabendo que esse nao e o processo real...
            case 1:
                autonomia = "" +super.getCilindrada()*3;     // equacao errada, apenas pretendo obter um valor para a autonomia
                break;
            case 2:
                autonomia = "0km";
            case 3:
                autonomia = "" + super.getCilindrada()*2;
        }
        return autonomia;
    }

    @Override
    public String toString() {
        switch(tipo) {
            case 1:
                return "Automóvel{" + super.toString() + ", emissão de CO2= " + emissao + "g/km" + "}";
            case 2:
                return "Automóvel{" + super.toString() + ", autonomia= " + autonomia + "km" + "}";
            case 3:
                return "Automóvel{" + super.toString() + ", emissão de CO2= " + emissao + "g/km, autonomia= " + autonomia + "km" + "}";
        }
        return "Automóvel{" + super.toString() + "}";
    }
}
