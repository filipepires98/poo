package aula07;

public class Velocipede extends Veiculo {
    public Velocipede(String matricula, String cor, int ano, int rodas, int cilindrada, int velmax, int lotmax, boolean ae) {
        super(matricula, cor, ano, rodas, cilindrada, velmax, lotmax, ae);
        while (ano<1950 || ano>2017 || rodas > 3 || rodas < 2 || lotmax > 3 || lotmax < 1) {
            if (ano<1950 || ano>2017) {
                System.out.print("\nAno Inválido, defina-o novamente: ");
                super.setAno(sc.nextInt());
            }
            if (rodas > 3 || rodas < 2) {
                System.out.print("\nNúmero Inválido de rodas, defina-o novamente: ");
                super.setRodas(sc.nextInt());
            }
            if (lotmax > 3 || lotmax < 1) {
                System.out.print("\nNúmero Inválido de lotação máxima, defina-a novamente: ");
                super.setLotmax(sc.nextInt());
            }
        }
        if (cilindrada != 0) {
            System.out.println("Velocípedes não têm cilindrada!");
            super.setCilindrada(0);
        }
        if (ae == true) {
            System.out.println("Velocípedes não têm permissão para frequentarem autoestradas!");
            super.setAe(false);
        }
    }
    public Velocipede() {
        while (super.getAno()<1950 || super.getAno()>2017 || super.getRodas() > 3 || super.getRodas() < 2 || super.getLotmax() > 3 || super.getLotmax() < 1) {
            if (super.getAno()<1950 || super.getAno()>2017) {
                System.out.print("\nAno Inválido, defina-o novamente: ");
                super.setAno(sc.nextInt());
            }
            if (super.getRodas()> 3 || super.getRodas() < 2) {
                System.out.print("\nNúmero Inválido de rodas, defina-o novamente: ");
                super.setRodas(sc.nextInt());
            }
            if (super.getLotmax() > 3 || super.getLotmax() < 1) {
                System.out.print("\nNúmero Inválido de lotação máxima, defina-a novamente: ");
                super.setLotmax(sc.nextInt());
            }
        }
        if (super.getCilindrada() != 0) {
            super.setCilindrada(0);
        }
        if (super.isAe() == true) {
            System.out.println("Velocípedes não têm permissão para frequentarem autoestradas!");
            super.setAe(false);
        }
    }

    @Override
    public String toString() {
        return "Velocípede{" + super.toString() + "}";
    }
}
