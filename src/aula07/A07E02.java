package aula07;
import java.util.*;

public class A07E02 {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        Pessoa a = new Pessoa();
        System.out.println(a);
        boolean b1 = a.FilmeGosto();
        boolean b2 = a.SerieGosto();
        int i1 = a.NumIrmaos();
        int i2 = a.Mensalidade();
        int i3 = a.HorasLivres();
        System.out.println (b1 + ", " + b2 + ", " + i1 + " irmãos, " + i2 + "euros, " + i3 + "horas");
    }
}
