package aula02;
import java.util.Scanner;

public class A02E09 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Input Byte: ");
        int number = sc.nextInt();


        while (number < 128 && number >=-128) {
            int i;
            String bin = "";

            if ((int)number >= 0) {
                bin = bin + "0";
                for (i=6; i>=0; i--) {
                    if ((int)Math.pow(2,i) <= number) {
                        number = number - (int)Math.pow(2,i);
                        bin = bin + "1";
                    } else {
                        bin = bin + "0";
                    }
                }

            } else {
                number = -number;
                bin = bin + "1";
                for (i=6; i>=0; i--) {
                    if ((int)Math.pow(2,i) <= number) {
                        number = number - (int)Math.pow(2,i);
                        bin = bin + "1";
                    } else {
                        bin = bin + "0";
                    }
                }
            }

            System.out.println("Binary Representation: " + bin);
            System.out.print("Input New Byte: ");
            number = sc.nextInt();
        }
        System.out.print("Invalid Byte for 8 Bit Representation.");
        sc.close();
    }
}