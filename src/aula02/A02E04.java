package aula02;
import java.util.Scanner;
import java.util.Arrays;

public class A02E04 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("First number from list: ");
        int number = sc.nextInt();
        int first = number;
        double max = 0;
        double min = 1000000;
        double med = 0;
        int count = 0;

        int lista[] = new int[100];

        do {
            count++;

            if (number > max) {
                max = number;
            }
            if (number < min) {
                min = number;
            }

            lista[count - 1] = number;
            med += number;

            System.out.print("Next number from list: ");
            number = sc.nextInt();

        } while (number != first);

        med = med/count;

        System.out.println("Valor Máximo: " + max);
        System.out.println("Valor Mínimo: " + min);
        System.out.println("Média: " + med);
        System.out.println("Número de elementos da lista: " + count);
        sc.close();
    }
}
