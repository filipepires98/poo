package aula02;
import java.util.Scanner;

public class A02E03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = -1;
        int i;
        while (number <= 0) {
            System.out.print("Number: ");
            number = sc.nextInt();
        }

        int count = 0;
        for (i = 1; i <= number; i++) {
            if (number % i == 0) {
                count++;
            }
        }

        if (count == 2) {
            System.out.println("Prime.");
        } else {
            System.out.println("Not Prime");
        }
        sc.close();
    }
}
