package aula02;
import java.util.Scanner;

public class A02E01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double T, P, NF;

        do {
            System.out.print("\nNota da componente Teórica: ");
            T = sc.nextDouble();
            System.out.print("Nota da componente Prática: ");
            P = sc.nextDouble();
            //NF = 0.4 * T + 0.6 * P;
            NF = (Math.round((T * 0.4 + P * 0.6) * 100))/100;

            if (T < 7 || P < 7) {
                System.out.print("66 - Reprovado por Nota Mínima");
            } else {
                if (NF >= 10) {
                    System.out.print("Aprovado | NF = " + NF);
                } else {
                    System.out.print("Reprovado | NF = " + NF);
                }
            }

        } while ( T != 0 && P != 0);
        sc.close();
    }
}
