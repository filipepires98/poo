package aula02;
import java.util.Scanner;

public class A02E02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("N = ");
        int n = sc.nextInt();

        while (n >= 0) {
            System.out.println(n);
            n--;
        }
        sc.close();
    }
}
