package aula02;
import java.util.Scanner;

public class A02E06 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        boolean newgame;
        do {
            System.out.println("New Game");
            int aleatorio = (int)(Math.random() * 100) + 1;
            int count = 0;
            int tentativa;

            do {
                count++;
                System.out.print("Escreva a sua tentativa: ");
                tentativa = sc.nextInt();

                if (tentativa < aleatorio) {
                    System.out.println("Tentativa demasiado BAIXA.");
                } else if (tentativa > aleatorio) {
                    System.out.println("Tentativa demaisado ALTA.");
                }

            } while (tentativa != aleatorio);
            System.out.println("O número é: " + aleatorio + ". Acertou ao fim de " + count + " tentativa(s)!");

            System.out.print("Novo Jogo (s/n)? ");
            if ( sc.next().equalsIgnoreCase("s")) {
                newgame = true;
            } else {
                newgame = false;
            }
        } while (newgame);
        sc.close();
    }
}
