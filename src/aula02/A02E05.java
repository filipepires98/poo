package aula02;
import java.util.Scanner;

public class A02E05 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Input Year: ");
        int ano = sc.nextInt();
        boolean bissexto;

        if (ano % 4 == 0) {
            if (ano % 100 != 0) {
                bissexto = true;
            } else {
                if (ano % 400 == 0) {
                    bissexto = true;
                } else {
                    bissexto = false;
                }
            }
        } else {
            bissexto = false;
        }

        System.out.print("Input Month (number): ");
        int mes = sc.nextInt();

        switch (mes) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                System.out.print("Number of days: 31");
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                System.out.print("Number of days: 30");
                break;
            case 2:
                if (bissexto) {
                    System.out.print("Number of days: 29");
                } else {
                    System.out.print("Number of days: 28");
                }
        }
        sc.close();
    }
}
