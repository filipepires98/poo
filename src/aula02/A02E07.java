package aula02;
import java.util.Scanner;

public class A02E07 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double T, P;
        int i, NF;

        System.out.format("%10s%10s%10s\n", "T", "P", "NF");
        for (i = 1; i <= 16; i++) {
            T = (Math.round((Math.random() * 20)*100))/100;      // ver porque é que as décimas nao permanecem
            P = (Math.round((Math.random() * 20)*100))/100;

            if (T >= 7 && P >= 7) {
                NF = (int)((Math.round((T * 0.4 + P * 0.6) * 100))/100);
            } else {
                NF = 66;
            }

            System.out.format("%10s%10s%10s\n", T, P, NF);
        }
        sc.close();
    }
}
