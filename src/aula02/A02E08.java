package aula02;
import java.util.Scanner;

public class A02E08 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Escreva o byte: ");
        int number = sc.nextInt();
        int contagem = 0;
        int i;

        for (i=7; i>=0 ; i--) {
            if (Math.pow(2,i) <= number) {
                number = number - (int)Math.pow(2,i);
                contagem++;
            }
        }
        System.out.print("Bits a 1 = " + contagem + "; Bits a 0 = " + (8-contagem));
        sc.close();
    }
}