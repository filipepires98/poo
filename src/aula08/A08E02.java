package aula08;

import java.util.*;

public class A08E02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Conjunto c1 = new Conjunto();
        Conjunto c2 = new Conjunto();

        System.out.println("TESTE NUMERO 1");
        c1.insert(4); c1.insert(7); c1.insert(6); c1.insert(5);
        int[] test = {7, 3, 2, 5, 4, 6, 7};
        for (int el : test) {c2.insert(el);}
        c2.remove(3); c2.remove(5); c2.remove(6);
        System.out.println(c1);
        System.out.println(c2);
        System.out.println("Número de elementos em c1: " + c1.size());
        System.out.println("Número de elementos em c2: " + c2.size());
        System.out.println("c1 contém 6?: " + ((c1.contains(c1,6) ? "sim" : "não")));
        System.out.println("c2 contém 6?: " + ((c2.contains(c2,6) ? "sim" : "não")));
        System.out.println("União:" + c1.unir(c2));
        System.out.println("Interseção:" + c1.interset(c2));
        System.out.println("Diferença:" + c1.subtrair(c2));
        c1.empty();
        c2.empty();

        System.out.println("\nTESTE NUMERO 2");
        c1.insert(new Pessoa()); c1.insert(new Pessoa("Joao Alegria",21345876,"30-05-1998")); c1.insert(new Pessoa("Sergio Ribeiro",7533442,"01-03-1998"));
        c2.insert(new Pessoa()); c1.insert(new Pessoa("Jean",73584372,"20-07-1998")); c1.insert(new Pessoa("Luis",75347247,"04-05-1998"));
        c2.remove(c2.getArray().get(c2.getArray().size()-1));  // c2.getArray() gets array from Conjunto; .get() gets element from array on position c2.getArray().size()-1  (last pos)
        System.out.println(c1);
        System.out.println(c2);
        System.out.println("Número de elementos em c1: " + c1.size());
        System.out.println("Número de elementos em c2: " + c2.size());
        System.out.println("c1 contém João Alegria?: " + ((c1.contains(c1, new Pessoa("Joao Alegria",21345876,"30-05-1998")) ? "sim" : "não")));
        System.out.println("c2 contém João Alegria?: " + ((c2.contains(c2, new Pessoa("Joao Alegria",21345876,"30-05-1998")) ? "sim" : "não")));
        System.out.println("União:" + c1.unir(c2));
        System.out.println("Interseção:" + c1.interset(c2));
        System.out.println("Diferença:" + c1.subtrair(c2));
        c1.empty();
        c2.empty();
    }
}
