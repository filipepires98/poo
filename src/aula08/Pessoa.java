package aula08;
import aula07.IDemo123;
import java.util.Scanner;

public class Pessoa <T> implements IDemo123,Comparable<Pessoa> {
    private Scanner sc = new Scanner(System.in);
    private T nome;
    private int cc;
    private String data;

    public int getCc() {return cc;}
    public T getNome() {return nome;}
    public String getData() {return data;}

    public void setCc(int cc) {this.cc = cc;}
    public void setNome(T nome) {this.nome = nome;}
    public void setData(String data) {this.data = data;}

    public Pessoa() {                                          // pre-definido
        this.nome = (T)"Filipe Pires";
        this.cc = 13831291;
        this.data = "25-06-1998";
    }
    public Pessoa(T nome, int cc, String data) {
        this.nome = nome;
        this.cc = cc;
        this.data = data;
    }

    public boolean FilmeGosto(){
        System.out.println("Gosta da saga Star Wars? (s/n) ");
        if (sc.next().equals("s")) {
            return true;
        }
        return false;
    }
    public boolean SerieGosto(){
        System.out.println("Gosta da serie Mr.Robot? (s/n) ");
        if (sc.next().equals("s")) {
            return true;
        }
        return false;
    }
    public boolean FrequentaGinasio() {
        String[] Data = data.split("-");
        if (Integer.parseInt(Data[2]) > 1998 || Integer.parseInt(Data[2]) < 1900) {
            return false;
        }
        System.out.println("Frequenta ginásio? (s/n) ");
        if (!(sc.next().equals("s"))) {
            return false;
        }
        return true;
    }
    public boolean FrequentaUA() {
        String[] Data = data.split("-");
        if (Integer.parseInt(Data[2]) > 1998 || Integer.parseInt(Data[2]) < 1900) {
            return false;
        }
        System.out.println("Frequenta a UA? (s/n) ");
        if (!(sc.next().equals("s"))) {
            return false;
        }
        return true;
    }
    public int NumIrmaos() {
        int num = -1;
        while (num < 0 || num > 30) {
            System.out.println("Número de irmãos: ");
            num = sc.nextInt();
        }
        return num;
    }
    public int Mensalidade() {
        int mensalidade = 0;
        if (FrequentaUA()) {
            if (FrequentaGinasio()) {
                mensalidade = 150+30;
            } else {
                mensalidade = 150;
            }
        } else {
            if (FrequentaGinasio()) {
                mensalidade = 50+30;
            } else {
                mensalidade = 50;
            }
        }
        return mensalidade;
    }
    public int HorasLivres() {
        int hl = -1;
        while (hl < 0 || hl > 24*7) {
            System.out.println("Horas livres semanais: ");
            hl = sc.nextInt();
        }
        return hl;
    }

    public int compareTo(Pessoa p) {                               // importante! metodo de comparacao para utilizar (p.e.) no sort de arraylists
        if (p.getCc() > this.getCc()) {
            return -1;
        } else if (p.getCc() == this.getCc()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object o) {                                           // equals e hashcode importantes para diferenciar pessoas
        if (this == o) return true;
        if (!(o instanceof Pessoa)) return false;

        Pessoa<?> pessoa = (Pessoa<?>) o;

        if (cc != pessoa.cc) return false;
        if (nome != null ? !nome.equals(pessoa.nome) : pessoa.nome != null) return false;
        return data != null ? data.equals(pessoa.data) : pessoa.data == null;
    }

    @Override
    public int hashCode() {
        int result = nome != null ? nome.hashCode() : 0;
        result = 31 * result + cc;
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Pessoa{" + "nome= '" + nome + '\'' + ", cc= " + cc + ", data de nascimento= '" + data + '\'' + '}';
    }
}
