package aula08;

public class Carne extends Alimento{
    private String variedade = "";
    private String nome = "";

    public Carne() {
        while (variedade.length() <= 2) {
            System.out.print("Variedade da Carne (Vaca / Porco / Peru / Frango / Outro): ");
            variedade = sc.next();
        }
        System.out.print("Nome: ");
        nome = sc.next();
    }
    public Carne(double proteinas, double calorias, double peso, String variedade, String nome) {
        super(proteinas, calorias, peso);
        this.variedade = variedade;
        this.nome = nome;
    }

    public String getVariedade() {return variedade;}
    public void setVariedade(String variedade) {this.variedade = variedade;}
    public String getNome() {return nome;}
    public void setNome(String nome) {this.nome = nome;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Carne)) return false;
        if (!super.equals(o)) return false;
        Carne carne = (Carne) o;
        if (variedade != null ? !variedade.equals(carne.variedade) : carne.variedade != null) return false;
        return nome != null ? nome.equals(carne.nome) : carne.nome == null;
    }
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (variedade != null ? variedade.hashCode() : 0);
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()  {
        return "     " + nome + "{" + variedade + ", " + super.toString() + '}';
    }
}
