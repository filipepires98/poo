package aula08;
import java.util.*;

public class Prato {    // criei apenas uma class de pratos pois apercebi me que seria muito mais comodo diferenciar os pratos atraves do nome (ver a minha forma de resolucao no metodo toString() ) em vez de criar uma classe pra cada tipo
    Scanner sc = new Scanner(System.in);
    private String nome;
    private ArrayList<Alimento> ingredientes = new ArrayList<>();

    public Prato(String nome, ArrayList<Alimento> ingredientes) {
        this.nome = nome;
        this.ingredientes = ingredientes;
    }
    public Prato(ArrayList<Alimento> AllFood) {
        System.out.println("Nome do prato (nao escreva palavras do genero 'vegetariano' ou 'dieta' se não pretender criar um prato com essas características): ");
        nome = sc.next();

        boolean controlprato = true;
        while (controlprato) {
            boolean controlalimento = false;
            System.out.println("Que tipo de ingredientes pretende adicionar? (1 = Carnes; 2 = Peixes; 3 = Cereais; 4 = Legumes; 5 = Terminar Prato)");
            switch (sc.nextInt()) {
                case 1:
                    System.out.println("Que carne pretende adicionar? ");
                    String carne = sc.next();

                    for (Alimento c: AllFood) {
                        if (c instanceof Carne) {
                            if((((Carne)c).getNome()).equals(carne)) {
                                ingredientes.add(c);
                                controlalimento = true;
                            }
                        }
                    }
                    if (controlalimento == false) {
                        System.out.println("Ingrediente não disponível.");
                    }
                    break;
                case 2:
                    System.out.println("Que peixe pretende adicionar? ");
                    String peixe = sc.next();

                    for (Alimento c: AllFood) {
                        if (c instanceof Peixe) {
                            if((((Peixe)c).getNome()).equals(peixe)) {
                                ingredientes.add(c);
                                controlalimento = true;
                            }
                        }
                    }
                    if (controlalimento == false) {
                        System.out.println("Ingrediente não disponível.");
                    }
                    break;
                case 3:
                    System.out.println("Que cereal pretende adicionar? ");
                    String cereal = sc.next();

                    for (Alimento c: AllFood) {
                        if (c instanceof Cereal) {
                            if((((Cereal)c).getTipo()).equals(cereal)) {
                                ingredientes.add(c);
                                controlalimento = true;
                            }
                        }
                    }
                    if (controlalimento == false) {
                        System.out.println("Ingrediente não disponível.");
                    }
                    break;
                case 4:
                    System.out.println("Que legume pretende adicionar? ");
                    String legume = sc.next();

                    for (Alimento c: AllFood) {
                        if (c instanceof Legume) {
                            if((((Legume)c).getTipo()).equals(legume)) {
                                ingredientes.add(c);
                                controlalimento = true;
                            }
                        }
                    }
                    if (controlalimento == false) {
                        System.out.println("Ingrediente não disponível.");
                    }
                    break;
                case 5:
                    controlprato = false;
                    System.out.println("Prato Concluído.");
                    break;
            }
        }
    }

    public String getNome() {return nome;}
    public void setNome(String nome) {this.nome = nome;}
    public ArrayList<Alimento> getIngredientes() {return ingredientes;}
    public void setIngredientes(ArrayList<Alimento> ingredientes) {this.ingredientes = ingredientes;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Prato)) return false;
        Prato prato = (Prato) o;
        if (sc != null ? !sc.equals(prato.sc) : prato.sc != null) return false;
        if (nome != null ? !nome.equals(prato.nome) : prato.nome != null) return false;
        return ingredientes != null ? ingredientes.equals(prato.ingredientes) : prato.ingredientes == null;
    }
    @Override
    public int hashCode() {
        int result = sc != null ? sc.hashCode() : 0;
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        result = 31 * result + (ingredientes != null ? ingredientes.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        boolean booleanveggie = true;
        double cal = 0;
        for (Alimento a: ingredientes) {
            if (!(a instanceof IVeggie)) {
                booleanveggie = false;
            }
            cal = cal + a.getCalorias();
        }

        if (booleanveggie) {
            if (cal <= 500) {           // 500 cal por 100g do prato é um numero inventado apenas pra responder a necessidade imediata do programa...
                return "Prato *Dieta e Vegetariano*" + ingredientes;
            } else {
                return "Prato *Vegetariano*" + ingredientes;
            }
        } else if (cal <= 500){
            return "Prato *Dieta*" + ingredientes;
        } else {
            return "Prato" + ingredientes;
        }
    }
}
