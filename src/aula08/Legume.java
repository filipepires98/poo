package aula08;

public class Legume extends Alimento implements IVeggie{
    private String tipo = "";

    public Legume() {
        System.out.print("Nome do Legume: ");
        tipo = sc.next();
    }
    public Legume(double proteinas, double calorias, double peso, String tipo) {
        super(proteinas, calorias, peso);
        this.tipo = tipo;
    }

    public String getTipo() {return tipo;}
    public void setTipo(String tipo) {this.tipo = tipo;}

    public void isVeggie() {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Legume)) return false;
        if (!super.equals(o)) return false;
        Legume legume = (Legume) o;
        return tipo != null ? tipo.equals(legume.tipo) : legume.tipo == null;
    }
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (tipo != null ? tipo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "     " + tipo + super.toString();
    }
}
