package aula08;
import java.util.*;

public class A08E01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        List<Pessoa> c1 = new ArrayList<>();
        for (int i = 10; i <= 100; i+=10){
            c1.add(new Pessoa("nome"+i,1000-i,i/10+"-06-1998"));
            System.out.println("Elemento" + (i+1) + ": " + c1.get(i/10 - 1));
        }
        System.out.println("Número de Pessoas: " + c1.size());

        c1.remove(c1.size()-1);
        c1.remove(c1.size()-1);
        System.out.println("Duas Pessoas Removidas");
        for (int i = 0; i < c1.size(); i++)
            System.out.println("Elemento" + (i+1) + ": " + c1.get(i));

        System.out.print("A Pessoa '" + c1.get(1) + "' ");
        c1.get(1).FilmeGosto();

        c1.sort(null);
        System.out.println(c1);

    }
}
