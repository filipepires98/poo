package aula08;
import java.util.*;

public class Conjunto <T> {
    Scanner sc = new Scanner(System.in);
    private ArrayList<T> array = new ArrayList<>();

    public Conjunto() {
        this.array = array;
        /*
        int num;
        System.out.print("Adicione elementos ao conjunto (Insira 'stop' quando quiser terminar):\n- ");
        String control = sc.next();

        while (true) {
            if (control.equals("stop")) {
                break;
            }
            num = Integer.valueOf(control);             // converter string do numero para integer
            if (!(array.contains(num))) {
                array.add(num);
            } else {
                System.out.println("Número ja pertence ao conjunto.");
            }
            System.out.print("- ");
            control = sc.next();
        }
        */
    }

    public Conjunto(ArrayList<T> array) {
        this.array = array;
    }

    public ArrayList<T> getArray() {return array;}
    public void setArray(ArrayList<T> array) {this.array = array;}

    public void insert(T x) {
        if (!(array.contains(x))) {
            array.add(x);
        }
    }

    public boolean contains(Conjunto c,T y) {
        int contain = 0;
        for (Object a : c.getArray()) {
            if (a instanceof Integer && y instanceof Integer) {
                if (a == y) {
                    contain = 1;
                }
            } else {
                if (a.equals(y)) {
                    contain = 1;
                }
            }
        }
        if (contain == 1) {
            return true;
        } else {
            return false;
        }
    }

    public void remove(T z) {
        if (array.contains(z)) {
            array.remove(z);
        }
    }

    public void empty() {
        array.clear();
    }

    @Override
    public String toString() {
        return "Conjunto{" + " array = " + array + '}';
    }

    public int size() {
        int size = array.size();
        return size;
    }

    public ArrayList<T> unir(Conjunto x) {
        ArrayList<T> c = new ArrayList<>();
        ArrayList<T> c2 = x.getArray();
        for (T a : array) {
            c.add(a);
        }
        for (T b : c2) {
            if (!(c.contains(b))) {
                c.add(b);
            }
        }
        return c;
    }

    public ArrayList<T> interset(Conjunto x) {
        ArrayList<T> c = new ArrayList<>();
        ArrayList<T> c2 = x.getArray();
        for (T a : array) {
            if (c2.contains(a)) {
                if (!(c.contains(a))) {
                    c.add(a);
                }
            }
        }
        return c;
    }

    public ArrayList<T> subtrair(Conjunto x) {
        ArrayList<T> c = new ArrayList<>();
        ArrayList<T> c2 = x.getArray();
        for (T a : array) {
            if (!(c2.contains(a))) {
                if (!(c.contains(a))) {
                    c.add(a);
                }
            }
        }
        for (T b : c2) {
            if (!(array.contains(b))) {
                if (!(c.contains(b))) {
                    c.add(b);
                }
            }
        }
        return c;
    }
}
