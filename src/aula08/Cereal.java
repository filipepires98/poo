package aula08;

public class Cereal extends Alimento implements IVeggie{
    private String tipo = "";

    public Cereal() {
        System.out.print("Nome do Cereal: ");
        tipo = sc.next();
    }
    public Cereal(double proteinas, double calorias, double peso, String tipo) {
        super(proteinas, calorias, peso);
        this.tipo = tipo;
    }

    public String getTipo() {return tipo;}
    public void setTipo(String tipo) {this.tipo = tipo;}

    public void isVeggie() {}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cereal)) return false;
        if (!super.equals(o)) return false;
        Cereal cereal = (Cereal) o;
        return tipo != null ? tipo.equals(cereal.tipo) : cereal.tipo == null;
    }
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (tipo != null ? tipo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "     " + tipo + super.toString();
    }
}
