package aula08;
import java.util.*;
/**
 Created by FILIPE PIRES on 03-04-2017.
 Programa de teste de um sistema de Ementas.
 **/
public class A08E03 {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {

        //         ----------         BD criada automaticamente no inicio do programa (com alimentos basicos e alguns pratos incluidos no armazém inicial

        // alimentos e respetivos arrays
        ArrayList<Alimento> AllFood = new ArrayList<>();
        ArrayList<Carne> Carnes = new ArrayList<>();
        ArrayList<Peixe> Peixes = new ArrayList<>();
        ArrayList<Cereal> Cereais = new ArrayList<>();
        ArrayList<Legume> Legumes = new ArrayList<>();
        Carne picanha = new Carne(20,250,10,"vaca","picanha");
        Carne lombo = new Carne(20,240,10,"porco","lombo");
        Carnes.add(picanha);Carnes.add(lombo);
        Peixe bacalhau = new Peixe(80,350,5,"congelado","bacalhau");
        Peixe dourada = new Peixe(20,185,5,"congelado","dourada");
        Peixes.add(bacalhau);Peixes.add(dourada);
        Cereal milho = new Cereal(10.5,365,1,"milho");
        Cereal aveia = new Cereal(18.5,335,1,"aveia");
        Cereal arroz = new Cereal(2.5,130,2,"arroz");
        Cereal batata = new Cereal(2,77,2,"batata");
        Cereais.add(milho);Cereais.add(aveia); Cereais.add(arroz); Cereais.add(batata);
        Legume alface = new Legume(1,15,1,"alface");
        Legume cenoura = new Legume(1.2,45,1,"cenoura");
        Legume tomate = new Legume(0.9,18,1,"tomate");
        Legumes.add(alface);Legumes.add(cenoura);Legumes.add(tomate);
        AllFood.add(picanha);AllFood.add(lombo);AllFood.add(bacalhau);AllFood.add(dourada);AllFood.add(milho);
        AllFood.add(aveia);AllFood.add(arroz);AllFood.add(batata);AllFood.add(alface);AllFood.add(cenoura);AllFood.add(tomate);

        //pratos e respetivo array
        ArrayList<Prato> Pratos = new ArrayList<>();
        ArrayList<Prato> Ementa = new ArrayList<>();
        ArrayList<Alimento> BacalhauNatas = new ArrayList<>();BacalhauNatas.add(bacalhau);BacalhauNatas.add(batata);
        Prato bacalhaunatas = new Prato("bacalhau com natas",BacalhauNatas);
        Pratos.add(bacalhaunatas); Ementa.add(bacalhaunatas);

        ArrayList<Prato> MeuPrato = new ArrayList<>();

        boolean control1 = true;
        System.out.println("Programa de Gestão de Ementas");                   //                  ---------                    Menu do Programa
        while (control1 == true) {
            boolean control2 = true;
            System.out.println("O que procura? (1 = Adicionar Ingredientes; 2 = Pratos; 3 = Ementas; 4 = Fechar Programa)");
            switch (sc.nextInt()) {
                case 1:                         // adicionar ingredientes ao armazem
                    while (control2 == true) {
                        boolean exists = false;
                        System.out.println("O que pretende adicionar? (1 = Carnes; 2 = Peixes; 3 = Cereais; 4 = Legumes; 5 = Voltar Atrás)");
                        switch (sc.nextInt()) {
                            case 1:
                                Carne x = new Carne();
                                for (Carne c: Carnes) {
                                    if (!((x.getNome()).equals(c.getNome()))) {
                                        exists = true;
                                        c.setPeso(c.getPeso() + x.getPeso());
                                    }
                                }
                                if (exists == false) {Carnes.add(x);AllFood.add(x);} else {System.out.println("Carne já existente. Peso adicionado ao armazém.");}
                                break;
                            case 2:
                                Peixe y = new Peixe();
                                for (Peixe c: Peixes) {
                                    if (!((y.getNome()).equals(c.getNome()))) {
                                        exists = true;
                                        c.setPeso(c.getPeso() + y.getPeso());
                                    }
                                }
                                if (exists == false) {Peixes.add(y);AllFood.add(y);} else {System.out.println("Peixe já existente. Peso adicionado ao armazém.");}
                                break;
                            case 3:
                                Cereal z = new Cereal();
                                for (Cereal c: Cereais) {
                                    if (!((z.getTipo()).equals(c.getTipo()))) {
                                        exists = true;
                                        c.setPeso(c.getPeso() + z.getPeso());
                                    }
                                }
                                if (exists == false) {Cereais.add(z);AllFood.add(z);} else {System.out.println("Cereal já existente. Peso adicionado ao armazém.");}
                                break;
                            case 4:
                                Legume w = new Legume();
                                for (Legume c: Legumes) {
                                    if (!((w.getTipo()).equals(c.getTipo()))) {
                                        exists = true;
                                        c.setPeso(c.getPeso() + w.getPeso());
                                    }
                                }
                                if (exists == false) {Legumes.add(w);AllFood.add(w);} else {System.out.println("Legume já existente. Peso adicionado ao armazém.");}
                                break;
                            case 5:
                                control2 = false;
                                break;
                        }
                    }
                    break;
                case 2:                         // criar pratos disponiveis para ementas
                    while (control2 == true) {
                        boolean exists = false;
                        System.out.println("O que pretende fazer? (1 = Criar Prato; 2 = Apagar Prato; 3 = Seleccionar Prato; 4 = Adicionar Ingrediente a Prato Existente; 5 = Remover Ingrediente a Prato Existente; 6 = Voltar Atrás)");
                        switch (sc.nextInt()) {
                            case 1:
                                Prato x = new Prato(AllFood);
                                for (Prato p: Pratos) {
                                    if ((p.getNome()).equals(x.getNome())) {
                                        exists = true;
                                    }
                                }
                                if (exists == false) {Pratos.add(x);} else {System.out.println("Prato já existente.");}
                                break;
                            case 2:
                                boolean elim = false;
                                System.out.println("Nome do prato que pretende eliminar (porfavor escreva corretamente): ");
                                String nomeelim = sc.next();
                                for (Prato p: Pratos) {
                                    if((p.getNome()).equals(nomeelim)) {
                                        Pratos.remove(p);
                                        System.out.println("Prato eliminado.");
                                    }
                                }
                                if (elim == false) {
                                    System.out.println("Prato não encontrado.");
                                }
                                break;
                            case 3:
                                System.out.println("Nome do prato que pretende seleccionar (porfavor escreva corretamente com espaços): ");
                                String nomeprato = sc.next();
                                for (Prato p: Pratos) {
                                    if ((p.getNome()).equals(nomeprato)) {
                                        exists = true;
                                        if (MeuPrato.size() == 0) {
                                            MeuPrato.add(p);
                                            System.out.println("Prato seleccionado.");
                                        } else {
                                            System.out.println("Já tem um prato seleccionado, pretende substitui-lo? (s/n)");
                                            if (sc.next().equals("s") || sc.next().equals("S")) {
                                                MeuPrato.clear();
                                                MeuPrato.add(p);
                                                System.out.println("Prato substituído.");
                                            } else {
                                                MeuPrato.add(p);
                                                System.out.println("Prato acrescentado.");
                                            }
                                        }

                                    }
                                }
                                if (exists == false) {
                                    System.out.println("Prato não se encontra disponível.");
                                }
                                break;
                            case 4:
                                boolean existsprato = false;
                                System.out.println("Nome do prato a que pretende adicionar ingredientes (porfavor escreva corretamente com espaços): ");
                                nomeprato = sc.next();
                                System.out.println("Ingrediente que pretende adicionar: ");
                                String nomeingrediente = sc.next();

                                for (Prato p: Pratos) {                                                                // iterar sobre pratos existentes
                                    if ((p.getNome()).equals(nomeprato)) {
                                        existsprato = true;                                                       // encontra prato
                                        for (Alimento a: AllFood) {                                                    // iterar sobre alimentos existentes
                                            if (a instanceof Carne) {
                                                if ((((Carne)a).getNome()).equals(nomeingrediente)) {
                                                    exists = true;                                                // encontra alimento (processo igual pra td tipo de alimento
                                                    if (p.getIngredientes().contains(a)) {
                                                        System.out.println("Prato já contém o ingrediente.");
                                                    } else {
                                                        (p.getIngredientes()).add(a);                                  // adiciona ingrediente
                                                        System.out.println("Ingrediente adicionado.");
                                                    }
                                                }
                                            } else if (a instanceof Peixe) {
                                                if ((((Peixe)a).getNome()).equals(nomeingrediente)) {
                                                    exists = true;
                                                    if (p.getIngredientes().contains(a)) {
                                                        System.out.println("Prato já contém o ingrediente.");
                                                    } else {
                                                        (p.getIngredientes()).add(a);
                                                        System.out.println("Ingrediente adicionado.");
                                                    }
                                                }
                                            } else if (a instanceof Cereal) {
                                                if ((((Cereal) a).getTipo()).equals(nomeingrediente)) {
                                                    exists = true;
                                                    if (p.getIngredientes().contains(a)) {
                                                        System.out.println("Prato já contém o ingrediente.");
                                                    } else {
                                                        (p.getIngredientes()).add(a);
                                                        System.out.println("Ingrediente adicionado.");
                                                    }
                                                }
                                            } else {
                                                if ((((Legume)a).getTipo()).equals(nomeingrediente)) {
                                                    exists = true;
                                                    if (p.getIngredientes().contains(a)) {
                                                        System.out.println("Prato já contém o ingrediente.");
                                                    } else {
                                                        (p.getIngredientes()).add(a);
                                                        System.out.println("Ingrediente adicionado.");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (existsprato == false) {
                                    System.out.println("Prato não disponível.");
                                } else {
                                    if (exists = false) {
                                        System.out.println("Ingrediente não disponível.");
                                    }
                                }
                                break;
                            case 5:
                                existsprato = false;
                                System.out.println("Nome do prato a que pretende remover ingredientes (porfavor escreva corretamente com espaços): ");
                                nomeprato = sc.next();
                                System.out.println("Ingrediente que pretende remover: ");
                                nomeingrediente = sc.next();

                                for (Prato p: Pratos) {                                                                // iterar sobre pratos existentes
                                    if ((p.getNome()).equals(nomeprato)) {
                                        existsprato = true;                                                       // encontra prato
                                        for (Alimento a: AllFood) {                                                    // iterar sobre alimentos existentes
                                            if (a instanceof Carne) {
                                                if ((((Carne)a).getNome()).equals(nomeingrediente)) {
                                                    exists = true;                                                // encontra alimento (processo igual pra td tipo de alimento
                                                    if (p.getIngredientes().contains(a)) {
                                                        (p.getIngredientes()).remove(a);                               // remove ingrediente
                                                        System.out.println("Ingrediente removido.");
                                                    } else {
                                                        System.out.println("Prato não contém ingrediente.");
                                                    }
                                                }
                                            } else if (a instanceof Peixe) {
                                                if ((((Peixe)a).getNome()).equals(nomeingrediente)) {
                                                    exists = true;
                                                    if (p.getIngredientes().contains(a)) {
                                                        (p.getIngredientes()).remove(a);
                                                        System.out.println("Ingrediente removido.");
                                                    } else {
                                                        System.out.println("Prato não contém ingrediente.");
                                                    }
                                                }
                                            } else if (a instanceof Cereal) {
                                                if ((((Cereal) a).getTipo()).equals(nomeingrediente)) {
                                                    exists = true;
                                                    if (p.getIngredientes().contains(a)) {
                                                        (p.getIngredientes()).remove(a);
                                                        System.out.println("Ingrediente removido.");
                                                    } else {
                                                        System.out.println("Prato não contém ingrediente.");
                                                    }
                                                }
                                            } else {
                                                if ((((Legume)a).getTipo()).equals(nomeingrediente)) {
                                                    exists = true;
                                                    if (p.getIngredientes().contains(a)) {
                                                        (p.getIngredientes()).remove(a);
                                                        System.out.println("Ingrediente removido.");
                                                    } else {
                                                        System.out.println("Prato não contém ingrediente.");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (existsprato == false) {
                                    System.out.println("Prato não disponível.");
                                } else {
                                    if (exists = false) {
                                        System.out.println("Ingrediente não disponível.");
                                    }
                                }
                                break;
                            case 6:
                                control2 = false;
                                break;
                        }
                    }
                    break;
                case 3:                            // gerir ementas
                    while (control2 == true) {
                        System.out.println("O que pretende fazer? (1 = Adicionar Prato à Ementa; 2 = Remover Prato da Ementa; 3 = Imprimir Ementa; 4 = Voltar Atrás)");
                        switch (sc.nextInt()) {
                            case 1:
                                boolean pratoexists = false;
                                System.out.println("Nome do prato que pretende adicionar (porfavor escreva corretamente com espaços): ");
                                String nomeprato = sc.next();
                                for (Prato p: Pratos) {
                                    if ((p.getNome()).equals(nomeprato)) {
                                        pratoexists = true;
                                        if (Ementa.contains(p)) {
                                            System.out.println("Ementa já contém esse prato.");
                                        } else {
                                            Ementa.add(p);
                                            System.out.println("Prato adicionado.");
                                        }
                                    }
                                }
                                if (!pratoexists) {
                                    System.out.println("Prato não disponível. Crie o prato antes de o adicionar à ementa.");
                                }
                                break;
                            case 2:
                                pratoexists = false;
                                System.out.println("Nome do prato que pretende remover (porfavor escreva corretamente com espaços): ");
                                nomeprato = sc.next();
                                for (Prato p: Pratos) {
                                    if ((p.getNome()).equals(nomeprato)) {
                                        pratoexists = true;
                                        if (Ementa.contains(p)) {
                                            Ementa.remove(p);
                                            System.out.println("Prato removido.");
                                        } else {
                                            System.out.println("Ementa não contém o prato.");
                                        }
                                    }
                                }
                                if (!pratoexists) {
                                    System.out.println("Prato não disponível. Ementa não pode conter um prato indisponível.");
                                }
                                break;
                            case 3:
                                //Criacao de arrays provisorios de forma a dividir tipos de pratos
                                ArrayList<Prato> normais = new ArrayList<>();
                                ArrayList<Prato> veggies = new ArrayList<>();
                                ArrayList<Prato> diets = new ArrayList<>();
                                ArrayList<Prato> both = new ArrayList<>();

                                //Criacao de array de controlo dos pratos ja impressos
                                ArrayList<Prato> e1 = new ArrayList<>();

                                //Criacao de arraylist com os arraylists de cada tipo
                                ArrayList<ArrayList> typearray = new ArrayList<>();

                                for (Prato p: Ementa) {
                                    if ((p.toString()).contains("Vegetariano") && (p.toString()).contains("Dieta")) {
                                        both.add(p);
                                    } else if((p.toString()).contains("Vegetariano") && !((p.toString()).contains("Dieta"))) {
                                        veggies.add(p);
                                    } else if (!((p.toString()).contains("Vegetariano")) && (p.toString()).contains("Dieta")) {
                                        diets.add(p);
                                    } else {
                                        normais.add(p);
                                    }
                                }
                                typearray.add(normais);typearray.add(veggies);typearray.add(diets);typearray.add(both);

                                System.out.println(" ---------------------------------------- EMENTA ---------------------------------------- ");
                                int index = -1;
                                for (ArrayList type: typearray) {                                                      // iterar sobre cada tipo de prato
                                    index++;
                                    System.out.println("\n -                                                                                      - ");
                                    double menoscal = 1000000;
                                    double calprato;
                                    int numerodepratos;
                                    switch (index) {
                                        case 0:
                                            numerodepratos = normais.size();
                                            System.out.println(" - Pratos Principais - ");
                                            while (numerodepratos > 0) {                                              // remover prato da lista provisoria sempre que ja tiver sido impresso ate não haverem mais
                                                for (Prato p: normais) {                                          // determinar prato menos calórico
                                                    calprato = 0;
                                                    for (Alimento a: p.getIngredientes()) {
                                                        calprato = calprato + a.getCalorias();
                                                    }
                                                    if (calprato < menoscal) {
                                                        menoscal = calprato;
                                                    }
                                                }
                                                for (Prato p: normais) {                                         // imprimir prato menos calórico
                                                    calprato = 0;
                                                    if (!(e1.contains(p))) {
                                                        for (Alimento a: p.getIngredientes()) {
                                                            calprato = calprato + a.getCalorias();
                                                        }
                                                        if (calprato == menoscal) {
                                                            System.out.print(" - " + p.getNome() + "\n");
                                                            for (Alimento a: p.getIngredientes()){
                                                                System.out.print(a);
                                                            }
                                                            e1.add(p);
                                                        }
                                                    }
                                                }
                                                numerodepratos--;
                                            }
                                            break;
                                        case 1:
                                            numerodepratos = veggies.size();
                                            System.out.println(" - Pratos Vegetarianos - ");
                                            while (numerodepratos > 0) {
                                                for (Prato p: veggies) {
                                                    calprato = 0;
                                                    for (Alimento a: p.getIngredientes()) {
                                                        calprato = calprato + a.getCalorias();
                                                    }
                                                    if (calprato < menoscal) {
                                                        menoscal = calprato;
                                                    }
                                                }
                                                for (Prato p: veggies) {
                                                    calprato = 0;
                                                    if (!(e1.contains(p))) {
                                                        for (Alimento a: p.getIngredientes()) {
                                                            calprato = calprato + a.getCalorias();
                                                        }
                                                        if (calprato == menoscal) {
                                                            System.out.print(" - " + p.getNome() + "\n");
                                                            for (Alimento a: p.getIngredientes()){
                                                                System.out.print(a);
                                                            }
                                                            e1.add(p);
                                                        }
                                                    }
                                                }
                                                numerodepratos--;
                                            }
                                            break;
                                        case 2:
                                            numerodepratos = diets.size();
                                            System.out.println(" - Pratos Dieta - ");
                                            while (numerodepratos > 0) {
                                                for (Prato p: diets) {
                                                    calprato = 0;
                                                    for (Alimento a: p.getIngredientes()) {
                                                        calprato = calprato + a.getCalorias();
                                                    }
                                                    if (calprato < menoscal) {
                                                        menoscal = calprato;
                                                    }
                                                }
                                                for (Prato p: diets) {
                                                    calprato = 0;
                                                    if (!(e1.contains(p))) {
                                                        for (Alimento a: p.getIngredientes()) {
                                                            calprato = calprato + a.getCalorias();
                                                        }
                                                        if (calprato == menoscal) {
                                                            System.out.print(" - " + p.getNome() + "\n");
                                                            for (Alimento a: p.getIngredientes()){
                                                                System.out.print(a);
                                                            }
                                                            e1.add(p);
                                                        }
                                                    }
                                                }
                                                numerodepratos--;
                                            }
                                            break;
                                        case 3:
                                            numerodepratos = both.size();
                                            System.out.println(" - Pratos Vegetarianos e de Dieta - ");
                                            while (numerodepratos > 0) {
                                                for (Prato p: both) {
                                                    calprato = 0;
                                                    for (Alimento a: p.getIngredientes()) {
                                                        calprato = calprato + a.getCalorias();
                                                    }
                                                    if (calprato < menoscal) {
                                                        menoscal = calprato;
                                                    }
                                                }
                                                for (Prato p: both) {
                                                    calprato = 0;
                                                    if (!(e1.contains(p))) {
                                                        for (Alimento a: p.getIngredientes()) {
                                                            calprato = calprato + a.getCalorias();
                                                        }
                                                        if (calprato == menoscal) {
                                                            System.out.print(" - " + p.getNome() + "\n");
                                                            for (Alimento a: p.getIngredientes()){
                                                                System.out.print(a);
                                                            }
                                                            e1.add(p);
                                                        }
                                                    }
                                                }
                                                numerodepratos--;
                                            }
                                            break;
                                    }
                                }
                                System.out.println(" -                                                                                      - ");
                                System.out.println(" ---------------------------------------------------------------------------------------- \n \n");
                                break;
                            case 4:
                                control2 = false;
                                break;
                        }
                    }
                    break;
                case 4:
                    control1 = false;
                    break;
                default:
                    System.out.println("Alerta: Input Inválido.");
                    break;
            }
        }
        System.out.println("Tenha um bom dia!");
    }
}
