package aula08;

public class Peixe extends Alimento {
   private String tipo = "";
   private String nome = "";

    public Peixe() {
        while (!(tipo.equals("congelado")) && !(tipo.equals("fresco"))) {
            System.out.print("Tipo de Peixe (congelado / fresco): ");
            tipo = sc.next();
        }
        System.out.print("Nome: ");
        nome = sc.next();
    }
    public Peixe(double proteinas, double calorias, double peso, String tipo, String nome) {
        super(proteinas, calorias, peso);
        this.tipo = tipo;
        this.nome = nome;
    }

    public String getTipo() {return tipo;}
    public void setTipo(String tipo) {this.tipo = tipo;}
    public String getNome() {return nome;}
    public void setNome(String nome) {this.nome = nome;}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Peixe)) return false;
        if (!super.equals(o)) return false;
        Peixe peixe = (Peixe) o;
        if (tipo != null ? !tipo.equals(peixe.tipo) : peixe.tipo != null) return false;
        return nome != null ? nome.equals(peixe.nome) : peixe.nome == null;
    }
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (tipo != null ? tipo.hashCode() : 0);
        result = 31 * result + (nome != null ? nome.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "     " + nome + "{" + tipo + ", " + super.toString() + "}\n";
    }
}
