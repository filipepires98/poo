package aula08;
import java.util.*;

public abstract class Alimento {
    Scanner sc = new Scanner(System.in);
    private double proteinas, calorias, peso;

    public Alimento() {
        System.out.print("Proteinas (por 100g): ");
        proteinas = sc.nextDouble();
        System.out.print("Calorias (por 100g): ");
        calorias = sc.nextDouble();
        System.out.print("Peso (kg): ");
        peso = sc.nextDouble();
    }
    public Alimento(double proteinas, double calorias, double peso) {
        this.proteinas = proteinas;
        this.calorias = calorias;
        this.peso = peso;
    }

    public double getProteinas() {return proteinas;}
    public void setProteinas(double proteinas) {this.proteinas = proteinas;}
    public double getCalorias() {return calorias;}
    public void setCalorias(double calorias) {this.calorias = calorias;}
    public double getPeso() {return peso;}
    public void setPeso(double peso) {this.peso = peso;}

    public int compareTo(Alimento a) {
        if (a.getCalorias() > this.getCalorias()) {
            return -1;
        } else if (a.getCalorias() == this.getCalorias()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Alimento)) return false;
        Alimento alimento = (Alimento) o;
        if (Double.compare(alimento.proteinas, proteinas) != 0) return false;
        if (Double.compare(alimento.calorias, calorias) != 0) return false;
        return Double.compare(alimento.peso, peso) == 0;
    }
    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(proteinas);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(calorias);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(peso);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "{" + "proteinas (por 100g)= " + proteinas + ", calorias (por 100g)= " + calorias + "cal}";
    }
}
