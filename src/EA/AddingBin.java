package EA;
import java.util.Scanner;


public class AddingBin {                                         // HOW TO I MAKE IT WORK FOR BOTH POSITIVE AND NEGATIVE BINARY NUMBERS ???
    public static void main (String[] args) {
        Scanner sc = new Scanner(System.in);

        String result;

        System.out.println("This is a testing adding machine.\nYou may choose whether to input decimal digits or binary digits.");
        System.out.print("Are you going to input binary numbers? (y/n) ");
        String answer = sc.next();

        if (answer.equals("y") || answer.equals("Y")) {
            System.out.println("Please, input 8 digit binary numbers. The machine will crash if you do not do so.");
            System.out.print("Input first number: ");
            String a = sc.next();
            System.out.print("Input second number: ");
            String b = sc.next();
            int i;
            String reversed = "";
            int carry = 0;

            for (i = 7; i >= 0; i--) {
                char aa = a.charAt(i);
                char bb = b.charAt(i);
                int aaa = (int)aa - 48;      // - 48 because (for some reason) char aa = int 49
                int bbb = (int)bb - 48;

                if (aaa + bbb + carry == 0) {
                    reversed = reversed + "0";
                    carry = 0;
                } else if (aaa + bbb + carry == 1) {
                    reversed = reversed + "1";
                    carry = 0;
                } else if (aaa + bbb + carry == 2){
                    reversed = reversed + "0";
                    carry = 1;
                    if (i == 0) {
                        reversed = reversed + "1";
                    }
                } else {
                    reversed = reversed + "1";
                    carry = 1;
                    if (i == 0) {
                        reversed = reversed + "1";
                    }
                }
            }
            result = new StringBuffer(reversed).reverse().toString();
            System.out.print(a + " + " + b + " = " + result);

        } else if (answer.equals("n") || answer.equals("N")) {
            System.out.print("Input first number: ");
            int a = sc.nextInt();
            System.out.print("Input second number: ");
            int b = sc.nextInt();
            result = "" + (a+b);
            System.out.print(a + " + " + b + " = " + result);

        } else {
            System.out.print("Invalid Answer. Please run the program again and answer the question only with 'y' or 'n'.");
        }
        sc.close();
    }
}
