package aula04;
import java.util.*;

public class E02v2_aluno {
    public static Scanner sc = new Scanner(System.in);
    ArrayList<String> listaalunos = listainicial();

    public static ArrayList<String> listainicial () {
        ArrayList listaalunos = new ArrayList();
        String aluno01 = "85122.Filipe Pires.LEI.0";
        listaalunos.add(aluno01);
        return listaalunos;
    }

    public static ArrayList<String> inscrever (ArrayList<String> listaalunos) {
        String aluno = "";
        System.out.println("Student´s ID number: ");
        aluno = aluno + sc.nextLine();
        System.out.println("Name: ");
        aluno = aluno + "." + sc.nextLine();
        System.out.println("Course: ");
        aluno = aluno + "." + sc.nextLine() + ".0"; //0 = num de livros requisitados.
        if (listaalunos.contains(aluno)) {
            System.out.println("Student already registered.");
        } else {
            System.out.println("New Student added to Database.");
            listaalunos.add(aluno);
        }
        return listaalunos;
    }
    public static ArrayList<String> remover (ArrayList<String> listaalunos) {
        String aluno = "";
        System.out.println("Student´s ID number: ");
        aluno = aluno + sc.nextLine();
        for (String a : listaalunos) {
            if ( aluno.equals(a.substring(0,5)) ) {
                listaalunos.remove(a);
                System.out.println("Student was removed from our Database.");
                return listaalunos;
            }
        }
        return listaalunos;
    }
    public static String printinscritos (ArrayList<String> listaalunos) {
        String listatotal = "";
        for(String d:listaalunos) {
            if (listaalunos.indexOf(d) == listaalunos.size()-1 ) {
                listatotal = listatotal + d + ".\n";
            } else {
                listatotal = listatotal + d + ";\n";
            }
        }
        return listatotal;
    }

}
