package aula04;
import java.util.*;

public class E02_livros {
    public static Scanner sc = new Scanner(System.in);
    ArrayList<String> listalivros = listainicial();


    // funcao chamada inicialmente para que a biblioteca tenha livros iniciais
    public static ArrayList<String> listainicial () {
        ArrayList listalivros = new ArrayList();
        listalivros.add("00001.The Hobbit.1");
        listalivros.add("00002.Ben Hur.1");
        listalivros.add("00003.The Holy Bible.1");
        listalivros.add("00004.English Dictionary.1");
        listalivros.add("00005.Os Maias.1");
        return listalivros;
    }

    // funcao chamada para registar livros novos
    public static ArrayList<String> registar (ArrayList<String> listalivros) {
        String livro = "";
        if (listalivros.size() <10) {                                             // creating new ID for the book
            livro = "0000" + (listalivros.size()+1);
        } else if (listalivros.size() <100 && listalivros.size() >= 10) {
            livro = "000" + (listalivros.size()+1);
        } else {
            livro = "00" + (listalivros.size()+1);
        }
        System.out.println("Book's Title: ");                                     // adding title to the book
        livro = livro + "." + sc.nextLine() + ".1";
        for (String l : listalivros) {
            if ((l.substring(6,l.length())).contains(livro.substring(6,livro.length()-2)+".0") || (l.substring(6,l.length())).contains(livro.substring(6,livro.length()))) {
                System.out.println("Book already registered.");
                return listalivros;
            }
        }
        System.out.println("Book not in our Database yet.\n");
        listalivros.add(livro);
        System.out.println("Book added to our Database.");
        return listalivros;
    }

    // funcao chamada para alterar disponibilidade do livro caso o emprestimo seja feito
    public static  ArrayList<String> emprestar01 (ArrayList<String> listalivros, ArrayList<String> listaalunos, String s, String b) {
        for (String l : listalivros) {
            for ( String a : listaalunos) {
                if (b.equals(l.substring(6,l.length()-2))) {                                          // se estamos a falar do mesmo livro
                    if (disponibilidade(listalivros, l.substring(6, l.length() - 2))) {               // se o livro estiver disponivel
                        if (s.equals(a.substring(0, 5))) {                                            // se o aluno estiver inscrito
                            if (!a.substring(a.length()).equals("3")) {                               // se não tiver o maximo de emprestimos pendentes
                                int index = listalivros.indexOf(l);
                                listalivros.set( index, l.substring(0, l.length() - 2) + ".0" );
                                System.out.println("You can now use the book. You have 7 days to deliver it back.");
                            } else {
                                System.out.println("You have 3 books waiting to be returned. Please return them first.");
                            }
                        } else {
                            System.out.println("Student isn´t in our Database.");
                        }
                    } else {
                        System.out.println("The book is not available right now, please come back in another day.");
                    }
                }
            }
        }

        return listalivros;
    }

    // funcao chamada para alterar numero de emprestimos pendentes do aluno caso o emprestimo seja feito
    public static  ArrayList<String> emprestar02 (ArrayList<String> listalivros, ArrayList<String> listaalunos, String s, String b) {
        for (String l : listalivros) {
            for ( String a : listaalunos) {
                if (b.equals(l.substring(6,l.length()-2))) {                                        // se estamos a falar do mesmo livro
                    if (disponibilidade(listalivros, l.substring(6, l.length() - 2)) == false) {    // se o livro ja nao estiver disponivel pela funcao anterior
                        if (s.equals(a.substring(0, 5))) {                                          // se o aluno estiver inscrito
                            if (!a.substring(a.length()).equals("3")) {                             // se não tiver o maximo de emprestimos pendentes
                                int index = listaalunos.indexOf(a);
                                if (a.substring(a.length() - 1).equals("2")) {
                                    listaalunos.set(index, a.substring(0, a.length() - 2) + ".3");
                                } else if (a.substring(a.length() - 1).equals("1")) {
                                    listaalunos.set(index, a.substring(0, a.length() - 2) + ".2");
                                } else {
                                    listaalunos.set(index, a.substring(0, a.length() - 2) + ".1");
                                }
                            }
                        }
                    }
                }

            }
        }
        return listaalunos;
    }

    // funcao chamada para alterar disponibilidade do livro caso a devolucao seja feita
    public static  ArrayList<String> devolver01 (ArrayList<String> listalivros, ArrayList<String> listaalunos, String s, String b) {
        for (String l : listalivros) {
            for ( String a : listaalunos) {
                if (b.equals(l.substring(6,l.length()-2))) {                                          // se estamos a falar do mesmo livro
                    if (!disponibilidade(listalivros, l.substring(6, l.length() - 2))) {              // se o livro nao estiver disponivel
                        if (s.equals(a.substring(0, 5))) {                                            // se o aluno estiver inscrito
                            if (!a.substring(a.length()).equals("0")) {                               // se o aluno tiver um livro pendente de devolucao
                                int index = listalivros.indexOf(l);
                                listalivros.set(index, l.substring(0, l.length() - 2) + ".1");
                                System.out.println("The book has been returned. Thank you, I hope it was helpful/interesting.");
                            }
                            else {
                                System.out.println("Student doesn´t have a book to return.");
                            }
                        } else {
                            System.out.println("Student isn´t in our Database.");
                        }
                    } else {
                        System.out.println("The library has that book available, please return the correct book if you have one.");
                    }
                }
            }
        }

        return listalivros;
    }

    // funcao chamada para alterar numero de emprestimos pendentes do aluno caso a devolucao seja feita
    public static  ArrayList<String> devolver02 (ArrayList<String> listalivros, ArrayList<String> listaalunos, String s, String b) {
        for (String l : listalivros) {
            for ( String a : listaalunos) {
                if (b.equals(l.substring(6,l.length()-2))) {                                        // se estamos a falar do mesmo livro
                    if (disponibilidade(listalivros, l.substring(6, l.length() - 2))) {             // se o livro ja estiver disponivel novamente pela funcao anterior
                        if (s.equals(a.substring(0, 5))) {                                          // se o aluno estiver inscrito
                            if (!a.substring(a.length()).equals("0")) {                             // se o aluno tiver um livro pendente de devolucao
                                int index = listaalunos.indexOf(a);
                                if (a.substring(a.length() - 1).equals("1")) {
                                    listaalunos.set(index, a.substring(0, a.length() - 2) + ".0");
                                } else if (a.substring(a.length() - 1).equals("2")) {
                                    listaalunos.set(index, a.substring(0, a.length() - 2) + ".1");
                                } else {
                                    listaalunos.set(index, a.substring(0, a.length() - 2) + ".2");
                                }
                            }
                        }
                    }
                }

            }
        }
        return listaalunos;
    }

    // funcao chamada para saber info para proceder-se ao emprestimo ou a devolucao
    public static String[] emprestardevolverinfo () {
        System.out.print("Student's ID: ");
        String s = sc.nextLine();
        System.out.print("Name of the book: ");
        String b = sc.nextLine();
        String [] emprestardevolverinfo = {s,b};
        return emprestardevolverinfo;
    }

    // funcao chamada para saber a disponibilidade do livro na biblioteca  (1 = disponivel)
    public static boolean disponibilidade (ArrayList<String> listalivros, String rbook) {
        for (String a : listalivros) {
            if (((a.substring(6,a.length()-2)).toLowerCase()).equals(rbook.toLowerCase())) {          // se estamos a falar do mesmo livro
                if (a.substring(a.length()-1).equals("1")) {                                          // se esta disponivel
                    return true;
                }
            }
        }
        return false;
    }

    // funcao chamada para fazer print da lista de livros disponíveis
    public static String printregistados (ArrayList<String> listalivros) {
        String listatotal = "";
        for(String d:listalivros) {
            if (d.substring(d.length()-1).equals("1")) {
               if (listalivros.indexOf(d) == listalivros.size() - 1) {
                   listatotal = listatotal + d + ".";
               } else {
                  listatotal = listatotal + d + ";\n";
              }
            }
        }
        return listatotal;
    }

    //funcao auxiliar
    public static String nextLine(String rbook) {
        if (rbook.equals("")) {
            System.out.println("Name of the book: ");
            rbook = rbook + sc.nextLine();
        }
        return rbook;
    }
}
