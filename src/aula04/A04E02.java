package aula04;
import java.util.*;

public class A04E02 {
	
	public static void main(String[] args) {

		int resposta, nmec, ID, lc=1;
		String nome, curso, titulo, emp;
		
		Map<Integer,E02_aluno> user = new HashMap<Integer, E02_aluno>();
		Map<String,E02_livro> livro = new HashMap<String, E02_livro>();
		
		Scanner sc = new Scanner(System.in);
		do{
			System.out.println("1 - inscrever aluno\n2 - remover aluno\n3 - imprimir lista de utilizadores\n4 - registar um novo livro\n5 - imprimir lista de livros\n6 - emprestar\n7 - devolver\n8 - verificar disponibilidade\n9 - sair");
			System.out.print("Resposta: ");
			resposta = sc.nextInt();
			switch(resposta){
			case 1:
				E02_aluno a = new E02_aluno();
				System.out.print("Nome: ");
				nome = sc.next();
				a.setNome(nome);
				System.out.print("Nmec: ");
				nmec = sc.nextInt();
				a.setNmec(nmec);
				System.out.print("Curso: ");
				curso = sc.next();
				a.setCurso(curso);
				user.put(nmec, a);
				break;
			
			case 2:
				do{
					System.out.print("Nmec do aluno: ");
					nmec = sc.nextInt();
				}while(!user.containsKey(nmec));
				user.remove(nmec);
				break;
				
			case 3:
				for(E02_aluno e : user.values()){
					System.out.println(e);
				}
				break;
			
			case 4:
				E02_livro l = new E02_livro();
				System.out.print("Titulo: ");
				titulo = sc.next();
				l.setTitulo(titulo);
				l.setID(lc);
				System.out.print("Tipo de emprestimo: ");
				emp = sc.next();
				l.setEmp(emp);
				livro.put(titulo, l);
				lc++;
				break;
				
			case 5:
				for(E02_livro e : livro.values()){
					System.out.println(e);
				}
				break;
				
			case 6:
				do{
					System.out.print("Nmec do aluno: ");
					nmec = sc.nextInt();
				}while(!user.containsKey(nmec));
				if(user.get(nmec).getControl()>=3)
					System.out.println("Utilizador ja tem o numero maximo de livros");
				else{
					do{
						System.out.print("Titulo do livro: ");
						titulo = sc.next();
					}while(!livro.containsKey(titulo));
					user.get(nmec).setLivro(livro.get(titulo));
					livro.remove(titulo);
				}
				break;
			
			case 7:
				int control = 0;
				do{
					System.out.print("Nmec do aluno: ");
					nmec = sc.nextInt();
				}while(!user.containsKey(nmec));
				System.out.print("Titulo do livro: ");
				titulo = sc.next();
				for(E02_livro e : user.get(nmec).getLivro()){
					if(e.getTitulo().equals(titulo)){
					for(E02_livro k : user.get(nmec).getLivro()){
						if((k.getTitulo()).equals(titulo)){
							control=1;
							livro.put(titulo, k);
							user.get(nmec).removeLivro(k);
						}	
					}
					if(control == 1)
						System.out.print("Livro invalido");
					}
				}
				break;
				
			case 8:
				System.out.print("Titulo do livro: ");
				titulo = sc.next();
				if (livro.containsKey(titulo))
					System.out.println("Livro esta disponivel para ser requesitado");
				else
					System.out.println("Livro nao disponivel");
			}
			
					
		}while(resposta!=9);
		
	}

}
