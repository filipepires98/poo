package aula04;

public class E01_RectangleClass {
    private int l;
    private int c;

    public void setL(int l) {
        this.l = l;
    }

    public void setC(int c) {
        this.c = c;
    }

    public int getL() {
        return l;
    }

    public int getC() {
        return c;
    }

    public int rectanglearea(int l, int c) {
        int area = (int)(l*c);
        return area;
    }
    public int rectangleper(int l, int c) {
        int per = (int)(2*l + 2*c);
        return per;
    }
}
