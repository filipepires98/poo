package aula04;
import java.util.ArrayList;
import java.util.List;

public class E02_aluno {

    private String nome, curso;
    private int nmec;
    private List<E02_livro> lista = new ArrayList<E02_livro>(3);
    private int controllivro=0;

    public E02_aluno(){
        this.nome = "*Nenhum atribuido*";
        this.curso = "*Nenhum atribuido*";
        this.nmec = 0;
    }

    public E02_aluno(String nome, String curso, int nmec){
        this.nome = nome;
        this.curso = curso;
        this.nmec = nmec;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getCurso() {
        return curso;
    }
    public void setCurso(String curso) {
        this.curso = curso;
    }
    public int getNmec() {
        return nmec;
    }
    public void setNmec(int nmec) {
        this.nmec = nmec;
    }

    public void setLivro(E02_livro livro){
        lista.add(livro);
        controllivro++;
    }

    public void removeLivro(E02_livro livro){
        lista.remove(livro);
        controllivro--;
    }

    public int getControl(){
        return controllivro;
    }

    public E02_livro[] getLivro(){
        E02_livro l[] = lista.toArray(new E02_livro[lista.size()]);
        return l;
    }


    public String toString(){
        E02_livro l[] = lista.toArray(new E02_livro[lista.size()]);
        return String.format("Nome: %s; Nmec: %s; Curso: %s", this.nome, this.nmec, this.curso);
    }

}
