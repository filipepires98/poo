package aula04;

public class E01_CircleClass {
    private int r;

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }

    public int circlearea(int r) {
        int area = (int)(Math.pow(r,2)*3.14);
        return area;
    }
    public int circleper(int r) {
        int per = (int)(r*2*3.14);
        return per;
    }
}
