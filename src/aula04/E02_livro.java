package aula04;

public class E02_livro {

    private String titulo, emp;
    private int ID;

    public E02_livro() {
        this.titulo = "*Nenhum atribuido*";
        this.emp = "*Nenhum atribuido*";
        this.ID = 0;
    }

    public E02_livro(String titulo, String emp, int iD) {
        this.titulo = titulo;
        this.emp = emp;
        this.ID = iD;
    }

    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public String getEmp() {
        return emp;
    }
    public void setEmp(String emp) {
        this.emp = emp;
    }
    public int getID() {
        return ID;
    }
    public void setID(int iD) {
        ID = iD;
    }

    public String toString(){
        return String.format("Titulo: %s/ ID: %d/ Emprestimo: %s", this.titulo, this.ID, this.emp);
    }

}
