package aula04;
import java.util.ArrayList;
import java.util.Scanner;

public class A04E02v2 {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {

        System.out.println("\nLibrary Testing System\n");

        ArrayList<String> alregist = E02v2_aluno.listainicial();
        ArrayList<String> bookregist = E02v2_livros.listainicial();
        String[] emprestardevolverinfo = new String[2];
        int decision;

        do {
            System.out.println("\nThe Program works through this menu. When asked what to do next, you should answer with a number from 1 to 9.\n1 - inscrever aluno\n" + "2 - remover aluno\n" + "3 - imprimir lista de utilizadores\n" + "4 - registar um novo livro\n" + "5 - imprimir lista de livros disponíveis\n" + "6 - pedir emprestado\n" + "7 - devolver\n" + "8 - verificar disponibilidade\n" + "9 - sair");
            System.out.println("\nWhat do you wish to do? ");
            decision = sc.nextInt();
            String rbook = "";

            switch (decision) {
                default:
                    break;
                case 1:
                    alregist = E02v2_aluno.inscrever(alregist);
                    break;
                case 2:
                    alregist = E02v2_aluno.remover(alregist);
                    break;
                case 3:
                    System.out.println(E02v2_aluno.printinscritos(alregist));
                    break;
                case 4:
                    E02v2_livros.registar(bookregist);
                    break;
                case 5:
                    System.out.println(E02v2_livros.printregistados(bookregist));
                    break;
                case 6:
                    emprestardevolverinfo = E02v2_livros.emprestardevolverinfo();
                    String s1 = emprestardevolverinfo[0];
                    String b1 = emprestardevolverinfo[1];

                    bookregist = E02v2_livros.emprestar01(bookregist,alregist,s1,b1);
                    alregist = E02v2_livros.emprestar02(bookregist,alregist,s1,b1);
                    break;
                case 7:
                    emprestardevolverinfo = E02v2_livros.emprestardevolverinfo();
                    String s2 = emprestardevolverinfo[0];
                    String b2 = emprestardevolverinfo[1];

                    bookregist = E02v2_livros.devolver01(bookregist,alregist,s2,b2);
                    alregist = E02v2_livros.devolver02(bookregist,alregist,s2,b2);
                    break;
                case 8:
                    rbook = E02v2_livros.nextLine(rbook);

                    if (E02v2_livros.disponibilidade(bookregist, rbook)) {
                        System.out.println("Book Available.");
                    } else {
                        System.out.println("Book NOT Available.");
                    }
                    break;
                case 9:
                    break;
            }


        } while (decision != 9);
        System.out.print("Come back any time. Have a nice day :)");
    }
}
