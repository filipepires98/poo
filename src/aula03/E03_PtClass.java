package aula03;
import java.util.*;

public class E03_PtClass {
    Scanner sc = new Scanner(System.in);
    public int x() {
        System.out.print("Input figure's center's x coordenate: ");
        return sc.nextInt();
    }
    public int y() {
        System.out.print("Input figure's center's y coordenate: ");
        return sc.nextInt();
    }
}
