package aula03;

public class E03_CircleClass {
    public int circlearea(int r) {
        int area = (int)(Math.pow(r,2)*3.14);
        return area;
    }
    public int circleper(int r) {
        int per = (int)(r*2*3.14);
        return per;
    }
}
