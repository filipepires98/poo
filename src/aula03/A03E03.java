

// ATENÇÃO!!! SE HOUVER ALGUM EXERCÍCIO 03 IDENTICO AO MEU PUBLICADO POR OUTRO ALUNO DE POO, A SUA RESOLUÇÃO FOI FEITA POR MIM E PARTILHADA COM ALUNOS A PEDIDO DE ALGUNS.



package aula03;
import java.util.*;

public class A03E03 {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("This is a geometrical figure creator program. Please input the name of the figure correctly.\nIf you type nothing and hit ENTER, the process will finish.");
        System.out.println("Which figure would you like to work on? (circle/rectangle/square)");
        String f1 = sc.nextLine();
        String f2 = f1;

        while (f1.equals("") == false) {

            String[] array = {f1, f2};
            int[] fig1 = new int[4];
            fig1[0] = -1000000;
            int[] fig2 = new int[4];
            int firstchange;                  // i created this int to know when to save the correct radium value (lines 33 and 37)

            for (String figure: array) {
                if (fig1[0] == -1000000) {
                    System.out.println("Figure Number One");
                } else {
                    System.out.println("Figure Number Two");
                }

                E03_PtClass PtClass = new E03_PtClass();
                int x = PtClass.x();
                int y = PtClass.y();
                if (fig1[0] == -1000000) {
                    fig1[0] = x;
                    fig1[1] = y;
                    firstchange = 0;
                } else {
                    fig2[0] = x;
                    fig2[1] = y;
                    firstchange = 1;
                }

                if (figure.equals("circle")) {
                    System.out.println("CIRCLE");
                    System.out.print("You're in Circle's Class now, type it's radium (int): ");
                    int r = sc.nextInt();

                    E03_CircleClass CircleClass = new E03_CircleClass();

                    int area = CircleClass.circlearea(r);
                    System.out.println("Your circle's area is: " + area);
                    int perimeter = CircleClass.circleper(r);
                    System.out.println("Your circle's perimeter is: " + perimeter);

                    if (firstchange == 0) {
                        fig1[2] = r;
                    } else {
                        fig2[2] = r;
                    }

                } else if (figure.equals("square")) {
                    System.out.println("SQUARE");
                    System.out.print("You're in Square's Class now, type it's side length (int): ");
                    int l = sc.nextInt();

                    E03_SquareClass SquareClass = new E03_SquareClass();

                    int area = SquareClass.squarearea(l);
                    System.out.println("Your square's area is: " + area);
                    int perimeter = SquareClass.squareper(l);
                    System.out.println("Your square's perimeter is: " + perimeter);

                    if (firstchange == 0) {
                        fig1[2] = l;
                    } else {
                        fig2[2] = l;
                    }

                } else if (figure.equals("rectangle")) {
                    System.out.println("RECTANGLE");
                    System.out.print("You're in Rectangle's Class now, type it's side height (int): ");
                    int l = sc.nextInt();
                    System.out.print("\nAnd it's side width (int): ");
                    int c = sc.nextInt();

                    E03_RectangleClass RectangleClass = new E03_RectangleClass();

                    int area = RectangleClass.rectanglearea(l, c);
                    System.out.println("Your rectangle's area is: " + area);
                    int perimeter = RectangleClass.rectangleper(l, c);
                    System.out.println("Your rectangle's perimeter is: " + perimeter);

                    if (firstchange == 0) {
                        fig1[2] = l;
                        fig1[3] = c;
                    } else {
                        fig2[2] = l;
                        fig2[3] = c;
                    }
                }
            } // end of foreach, beggining of intersections

            if (f1.equals("circle")) {
                int d = (int)Math.sqrt(Math.pow((fig2[0]-fig1[0]),2)+Math.pow((fig2[1]-fig1[1]),2));      // FOR SOME REASON DOES NOT WORK
                int max = fig1[2]+fig2[2];

                if (d > max) {
                    System.out.println("Circles do not intersect.\n");
                } else if (d == (fig1[2]+fig2[2])) {
                    System.out.println("Circles intersect in one point.\n");
                } else {
                    System.out.println("Circles intersect in more than one point.\n");
                }
            } else if (f1.equals("square")) {
                int sx1, sx2, sx3, sx4, sy1, sy2, sy3, sy4; // diagonals of both squares
                sx1 = fig1[0] - fig1[2]/2;
                sx2 = fig1[0] + fig1[2]/2;
                sx3 = fig2[0] - fig2[2]/2;
                sx4 = fig2[0] + fig2[2]/2;
                sy1 = fig1[1] - fig1[2]/2;
                sy2 = fig1[1] + fig1[2]/2;
                sy3 = fig2[1] - fig2[2]/2;
                sy4 = fig2[1] + fig2[2]/2;

                if (sx3 > sx2 || sy3 > sy2 || sx1 > sx4 || sy1 > sy4) {
                    System.out.println("Squares do not intersect.\n");
                } else {
                    System.out.println("Squares intersect.\n");
                }

            } else if (f1.equals("rectangle")){
                int sx1, sx2, sx3, sx4, sy1, sy2, sy3, sy4;  // diagonals of both rectangles
                sx1 = fig1[0] - fig1[2]/2;
                sx2 = fig1[0] + fig1[2]/2;
                sx3 = fig2[0] - fig2[2]/2;
                sx4 = fig2[0] + fig2[2]/2;
                sy1 = fig1[1] - fig1[3]/2;
                sy2 = fig1[1] + fig1[3]/2;
                sy3 = fig2[1] - fig2[3]/2;
                sy4 = fig2[1] + fig2[3]/2;

                if (sx3 > sx2 || sy3 > sy2 || sx1 > sx4 || sy1 > sy4) {
                    System.out.println("Rectangles do not intersect.\n");
                } else {
                    System.out.println("Rectangles intersect.\n");
                }

            } // end of intersections
        } // end of while
    } // end of main
}
