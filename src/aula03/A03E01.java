package aula03;
import java.util.Scanner;

public class A03E01 {
    public static Scanner sc = new Scanner(System.in);     // this makes Scanner public for every function
    public static void main(String[] args) {
        //Scanner sc = new Scanner(System.in);
        System.out.print("First Word: ");
        String word1 = sc.nextLine();
        System.out.print("Second Word: ");
        String word2 = sc.nextLine();

        int l1 = word1.length();
        int l2 = word2.length();
        String lcword1 = word1.toLowerCase();
        String lcword2 = word2.toLowerCase();

        function1(word1, word2, l1, l2, lcword1, lcword2);
        function2(word1, word2);
    }
    public static void function1(String word1, String word2, int l1, int l2, String lcword1, String lcword2) {
        String c2 = word2.substring(l2-1);

        System.out.printf("Number of characters:\nFirst word: %2d\nSecond word: %2d\n", l1, l2);
        System.out.println("First word's last character: " + word1.charAt(l1-1));
        if (word1.equals(word2)) {
            System.out.println("First word is exactly the same as the second word.");
        } else {
            System.out.println("First word is different from the second word.");
        }
        if (c2.equals(".")) {
            System.out.println("Second word's last character is '.'.");
        } else {
            System.out.println("Second word's last character isn't a dot, it's '" + word2.charAt(l2-1) + "'.");
        }
        if (lcword2.equals(word2)) {
            System.out.println("Second word is written in lowercase.");
        } else {
            System.out.println("Second word has characters in uppercase.");
        }
        System.out.println("First word in lowercase: " + lcword1);
        System.out.println("Second word in lowercase: " + lcword2);
    }
    public static void function2(String word1, String word2){
        String r1 = word1.replaceAll(" +", " ");   // o '+' dentro das primeiras aspas permite englobar todos os espaços q estejam seguidos COM REPLACEALL
        System.out.println("First word without multiple spaces: " + r1);
        String r2 = word2.replaceAll(" +", " ");   // temos o replace, replaceAll, replaceFirst, replaceLast, etc.
        System.out.println("Second word without multiple spaces: " + r2);
    }
}