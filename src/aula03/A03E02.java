package aula03;
import java.util.*;      // importing only scanner saves time but that is irrelevant in these type of exercises

public class A03E02 {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        //Scanner sc = new Scanner(System.in);
        System.out.println("Write your paragraph:");
        String p = sc.nextLine();
        paragraph(p);
    }
    public static void paragraph(String p) {
        String array[] = p.split("[!?.]+"); // inside [] u can add any symbol u wish for it 2 search. the + is for when there r multiple symbols (...)
        System.out.printf("\nYour paragraph has %d phrase(s).\n", array.length);

        for (String a : array) {
            if (a.startsWith(" ")) {
                String as = a.substring(1, a.length());
                System.out.println(as);
            } else if (a.isEmpty()) {
                break;
            } else {
                System.out.println(a);
            }
        }

    }
}
