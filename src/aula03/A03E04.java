package aula03;
import java.util.*;

public class A03E04 {
    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Calendar Creator");
        String[] meses = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
        int[] months = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
        int[] numdias = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; // We ignore leap years, for the sake of the exercise.

        int[] data = date();
        int ano = data[1];
        int mesInt = data[0];
        String mesStr = month(data, months, meses);
        int diasmes = monthdays(mesInt, months, numdias);
        int sem = data[2];

        calendar(mesStr, ano, sem, diasmes);

    }

    public static int[] date() {
        System.out.println("Choose the date (write numbers, hit enter when month is set, repeat when year is set).\nAlso set the day of the week you wish to be the first of the month.\n(1 = Monday; 2 = Tuesday; 3 = Wednesday; 4 = Thursday; 5 = Friday; 6 = Saturday; 7 = Sunday)");
        int[] data = {sc.nextInt(), sc.nextInt(), sc.nextInt()};
        return data;
    }

    public static String month(int[] data, int[] months, String[] meses) {
        String mesStr = "";
        int index = -1;
        for (int a : months) {
            index++;
            if (a == data[0]) {
                mesStr = meses[index];
            }
        }
        return mesStr;
    }

    public static int monthdays(int mesInt, int[] months, int[] numdias) {
        int diasmes = 0;
        int index = -1;
        for (int a : months) {
            index++;
            if (a == mesInt) {
                diasmes = numdias[index];
            }
        }
        return diasmes;
    }

    public static void calendar(String mesStr, int ano, int sem, int diasmes) {

        int[] array = new int[sem - 1 + diasmes];     // inclui 000 + tds os dias do mes
        int count = 0;
        while (count < diasmes) {
            array[sem - 1 + count] = count + 1;
            count++;
        }

        System.out.println(mesStr.toUpperCase() + " - " + ano);
        System.out.println("-----------------------\n| Mo Tu We Th Fr Sa Su|\n-----------------------");

        String semanal = "";
        count = -sem+1;
        int i;

        for (i = 0; i < (sem-1+diasmes); i++) {       // esta a percorrer todos os dias do array
            count++;                                  // comeca no -sem+2
            if (i < sem-1) {
                semanal = semanal + "   ";
            } else if (i < (sem+8)) {
                semanal = semanal + " 0" + count;
            } else {
                semanal = semanal + " " + count;
            }
            if (semanal.length() == 21) {
                System.out.println("|"+semanal+"|");
                semanal = "";
            }
        }
        if (semanal.length() != 0) {
            System.out.printf("|"+semanal+"%"+(23-semanal.length())+"s","|\n");
        }
        System.out.println("-----------------------");
    }
}
