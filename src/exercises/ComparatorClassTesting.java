package exercises;
import java.util.*;

public class ComparatorClassTesting implements Comparator<ClassToBeSorted> {
    ArrayList<ClassToBeSorted> base;
    public ComparatorClassTesting(ArrayList<ClassToBeSorted> base) {
        this.base = base;
    }

    @Override
    public int compare(ClassToBeSorted a, ClassToBeSorted b) {
        Integer ai = a.getNumber();
        Integer bi = b.getNumber();
        return ai.compareTo(bi);
    }


}
