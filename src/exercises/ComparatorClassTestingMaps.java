package exercises;
import java.util.*;

public class ComparatorClassTestingMaps implements Comparator<Integer>{
    TreeMap<Integer, ClassToBeSorted> base;
    public ComparatorClassTestingMaps(TreeMap<Integer,ClassToBeSorted> base) {
        this.base = base;
    }

    @Override
    public int compare(Integer a, Integer b) {
        return b.compareTo(a);
    }

}
