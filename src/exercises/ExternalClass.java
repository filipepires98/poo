package exercises;
import java.util.*;

public class ExternalClass {
    // non-static members
    private String name = "filipe";
    private int nmec = 100;
    private int cc = 99;
    private List<Integer> list = new ArrayList<>();
    // static members
    private static int number = 10;
    private static List<Integer> staticlist = new ArrayList<>();

    public class InternalClass {
        private int internalnumber = 1000;

        public void internalNumberPrinter() {
            System.out.println("Internal Number: " + internalnumber);
        }

        public void stringPrinter() {
            cc = 111;
            System.out.println("Dados: " + name + ", " + nmec + ", " + cc);
        }
    }

    public static class InternalStaticClass {
        public void numberPrinter() {
            System.out.println("Static Number: " + number);
        }
        public void arrayFiller () {
            for (int i=0; i<10; i++) {
                staticlist.add((int)(i*10*Math.random()));
            }
        }
        public void arrayPrinter() {
            System.out.println(staticlist);
        }
    }

    public ExternalClass(String name, int nmec, int cc, List<Integer> list) {
        this.name = name;
        this.nmec = nmec;
        this.cc = cc;
        this.list = list;
    }

    public ExternalClass() {
    }

    public void stringPrinter() {
        System.out.println("Dados: " + name + ", " + nmec + ", " + cc);
    }

    public int multiplicator() {
        class LocalClass {
            private int localnumber;
            public int multiplicacao() {
                localnumber = (int)(10*Math.random());
                return (int)cc*nmec*localnumber;
            }
        }
        LocalClass lc = new LocalClass();
        return lc.multiplicacao();
    }

}
