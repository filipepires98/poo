package exercises;

public class ClassToBeSorted implements Comparable<ClassToBeSorted> {
    private int number;
    private String name;

    public ClassToBeSorted(int number, String name) {
        this.number = number;
        this.name = name;
    }

    public int getNumber() {return number;}
    public void setNumber(int number) {this.number = number;}
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    @Override
    public String toString() {
        return "{" + number + ", " + name + '}';
    }

    @Override
    public int compareTo(ClassToBeSorted o) {
        //return name.compareTo(o.getName());
        if (o.getNumber() < number) {
            return 1;
        } else {
            return -1;
        }

    }
}
