package exercises;
import aula05.Quadrado;

import java.util.*;
import java.io.*;

public class Testing {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {

        /*
        System.out.printf("%-10s%-10s%-10s%-10.2s%-10.1f% f\n", "word1", "word2", "word3", "10.7", 11.45, 12.555);        // formating
        System.out.println("bla bla bla \t bla bla bla \n");        //

        ArrayList<String> a = new ArrayList<>();
        a.add("ola");
        for (String b: a) {
            if (b.contains("[a-zA-z]"))
            System.out.println(b);
        }

        System.out.println("\nShe said \"Hello!\" to me.\n");

        // Initialize Scanner object
        Scanner scan = new Scanner("Anna Mills Female 18b 21 a . yes");
        // initialize the string delimiter
        scan.useDelimiter("[ab]");
        // Printing the delimiter used
        System.out.println("The delimiter use is "+scan.delimiter());
        // Printing the tokenized Strings
        while(scan.hasNext()){
            System.out.println(scan.next());
        }
        // closing the scanner stream
        scan.close();
        */

        /*
        int[][] building = { {0, 0, 0}, {2, 3, 3, 2, 0}, {2, 1, 3} };
        System.out.println("# apartamentos: " + Arrays.deepToString(building)) ;

        String[] vetor = {"a","b","c","b","d","e"};
        Set<String> Set = new TreeSet<>();
        for (String elem: vetor) {
            Set.add(elem);
        }
        for (String element: Set) {
            System.out.print(element + ", ");
        }
        System.out.print("\n" + Set.toArray()[0]);
        System.out.println("\n");
        ArrayList<String> arraylist = new ArrayList<>();
        for (String v: vetor) {
            arraylist.add(v);
        }
        List<String> list = arraylist.subList(3,5);
        list.add("a");
        System.out.println(arraylist);
        System.out.println(list);

        Map<String, Integer> mapa = new TreeMap<>();
        mapa.put("s1",1);
        mapa.put("s2",2);
        System.out.println(mapa.entrySet());
        System.out.println(mapa.keySet());
        System.out.println(mapa.values());
        */

        /*
        System.out.println("\n");

        // ARRAYLISTS SORTING
        ArrayList<ClassToBeSorted> arrayToBeSorted = new ArrayList<>();
        for (int i=10; i>0; i--) {
            String s = "String" + i;
            ClassToBeSorted object1 = new ClassToBeSorted(i,s);
            arrayToBeSorted.add(object1);
        }
        System.out.println(arrayToBeSorted);

        ComparatorClassTesting cmp = new ComparatorClassTesting(arrayToBeSorted);
        ArrayList<ClassToBeSorted> arrayToBeSorted2 = new ArrayList<>();
        Collections.sort(arrayToBeSorted,cmp);
        System.out.println(arrayToBeSorted);

        //Collections.sort(arrayToBeSorted);
        //System.out.println(arrayToBeSorted);
        */
        // MAPS SORTING
        /*
        TreeMap<Integer,ClassToBeSorted> mapToBeSorted = new TreeMap<>();
        for (int i=10; i>0; i--) {
            String s = "String" + i;
            ClassToBeSorted object1 = new ClassToBeSorted(i,s);
            int integer = 100-i;
            mapToBeSorted.put(integer, object1);
        }
        System.out.println(mapToBeSorted);

        ComparatorClassTestingMaps cmp = new ComparatorClassTestingMaps(mapToBeSorted);
        TreeMap<Integer, ClassToBeSorted> sorted_mapToBeSorted = new TreeMap<>(cmp);
        sorted_mapToBeSorted.putAll(mapToBeSorted);
        System.out.println(sorted_mapToBeSorted);
        */

        /*
        System.out.println("\n");

        ExternalClass.InternalStaticClass internalStaticObject = new ExternalClass.InternalStaticClass();
        internalStaticObject.numberPrinter();
        internalStaticObject.arrayFiller();
        internalStaticObject.arrayPrinter();

        System.out.println("\n");

        ExternalClass externalObject = new ExternalClass();
        ExternalClass.InternalClass internalObject = externalObject.new InternalClass();
        externalObject.stringPrinter();
        internalObject.stringPrinter();
        externalObject.stringPrinter();
        internalObject.internalNumberPrinter();
        System.out.println("Uso da classe local: " + externalObject.multiplicator());

        System.out.println("\n");

        EnumSet<Enum> enumset = EnumSet.allOf(Enum.class);
        System.out.println("We are in " + enumset.toArray()[4]);
        Map<Enum, Integer> enummap = new TreeMap<Enum, Integer>();
        for (int control=1; control <= Enum.values().length; control++) {
            enummap.put(Enum.values()[control-1],control);
        }
        System.out.println(enummap);
        */

        /*
        System.out.println("\n");

        File diretorio = new File("Testing.java.newdir");
        diretorio.mkdirs();
        File ficheiro1 = new File(diretorio, "ficheiro1.txt");
        File ficheiro2 = new File(diretorio, "ficheiro2.txt");
        PrintWriter pw;
        // ficheiros e diretorios
        try {
            pw = new PrintWriter(ficheiro1);
            pw = new PrintWriter(ficheiro2);
            String[] arquivos = diretorio.list();
            for (String a: arquivos) {
                System.out.printf("%-15s",a);
            }
            System.out.println();
        } catch (FileNotFoundException e) {

        }
        // copiar ficheiro
        try {
            File inputFile = new File("input.txt");
            File outputFile = new File("output.txt");
            FileWriter fw = new FileWriter("input.txt");
            fw.write("ola");
            fw.close();
            FileReader in = new FileReader(inputFile);
            FileWriter out = new FileWriter(outputFile);
            int c;
            while ((c = in.read()) != -1)
                out.write(c);
            in.close();
            out.close();
        } catch (IOException e) {
            System.out.println("Erro!");
        }

        try {
            InputStreamReader isr = new InputStreamReader(System.in);
            char c;
            c = (char) isr.read();
            System.out.print(c);
        } catch (IOException e) {

        }
        */
        /*
        Quadrado a = new Quadrado();
        try {
            ByteArrayOutputStream mOut = new ByteArrayOutputStream();
            ObjectOutputStream serializer = new ObjectOutputStream(mOut);
            serializer.writeObject(a);
            serializer.flush();
        } catch (IOException i) {

        }
        */
    }
}
