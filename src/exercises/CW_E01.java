package exercises;
import java.util.Scanner;

public class CW_E01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        double[] arraynumeros = new double[100];     /* 100 = nº max de elementos do array */
        double number;
        int size = 0;     /* contagem dos elementos existentes no array */

        do {
            System.out.print("Insert Number: ");
            number = sc.nextDouble();
            arraynumeros[size] = number;
            size++;
        } while (number >= 0);

        int i = 0;

        while (i != (size - 1)) {         /* -1 pq ultimo elemento é o negativo */
            System.out.println(arraynumeros[i]);
            i++;
        }
        sc.close();
    }
}
