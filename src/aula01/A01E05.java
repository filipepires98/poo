package aula01;
import java.util.Scanner;

public class A01E05 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double segundos, res;
		int hr, min, sec;
		
		System.out.print("Segundos: ");
		segundos = sc.nextDouble();
		
		res = segundos % 3600;
		hr = (int)(segundos / 3600);
		
		sec = (int)(res % 60);
		min = (int)(res / 60);
		
		System.out.print(hr + ":" + min + ":" + sec);
		sc.close();
	}

}
