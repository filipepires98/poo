package aula01;
import java.util.Scanner;

public class A01E06 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double x1, x2, y1, y2, dist;
		
		System.out.println("Insira as coordenadas do ponto P1: ");
		System.out.print("x1 = ");
		x1 = sc.nextDouble();
		System.out.print("y1 = ");
		y1 = sc.nextDouble();
		
		System.out.println("Insira as coordenadas do ponto P2: ");
		System.out.print("x2 = ");
		x2 = sc.nextDouble();
		System.out.print("y2 = ");
		y2 = sc.nextDouble();
		
		double a = x2 - x1;
		double b = y2 - y1;
		dist = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
		System.out.print("Distância entre P1 e P2 = " + dist);
		
		sc.close();
	}

}
