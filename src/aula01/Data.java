package aula01;
import java.util.*;

public class Data {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        //String data = DataSet();
    }
    public static String DataSet(){
        int dia, mes, ano;
        String data;
        System.out.print("Dia: ");
        dia = sc.nextInt();
        System.out.print("Mes: ");
        mes = sc.nextInt();
        System.out.print("Ano: ");
        ano = sc.nextInt();

        if (dia < 10) {
            if (mes < 10) {
                data = "0" + dia + "-0" + mes + "-" + ano;
            } else {
                data = "0" + dia + "-" + mes + "-" + ano;
            }
        } else {
            if (mes < 10) {
                data = "" + dia + "-0" + mes + "-" + ano;
            } else {
                data = "" + dia + "-" + mes + "-" + ano;
            }
        }
        return data;
    }
    public static String Data(int dia, int mes, int ano) {
        String data;
        if (dia < 10) {
            if (mes < 10) {
                data = "0" + dia + "-0" + mes + "-" + ano;
            } else {
                data = "0" + dia + "-" + mes + "-" + ano;
            }
        } else {
            if (mes < 10) {
                data = "" + dia + "-0" + mes + "-" + ano;
            } else {
                data = "" + dia + "-" + mes + "-" + ano;
            }
        }
        return data;
    }
}
