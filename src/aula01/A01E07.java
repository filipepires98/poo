package aula01;
import java.util.Scanner;

public class A01E07 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		double ca, cb, hip, angulo;
		
		System.out.print("Cateto A: ");
		ca = sc.nextDouble();
		System.out.print("Cateto B: ");
		cb = sc.nextDouble();
		
		hip = Math.sqrt(Math.pow(ca, 2) + Math.pow(cb, 2));
		System.out.println("Hipotenusa = " + hip);
		
		angulo = Math.asin(ca/hip);
		System.out.print("Ângulo = " + angulo);
		
		sc.close();
	}

}
