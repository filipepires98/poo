package aula01;
import java.util.Scanner;

public class A01E03 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double cel, far;
		
		System.out.print("Insira a temperatura em ºC: ");
		cel = sc.nextDouble();
		
		far = 1.8*cel +32;
		System.out.print("A conversão para Farenheit é: " + far);
		sc.close();
	}

}
