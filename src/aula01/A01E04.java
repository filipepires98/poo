package aula01;
import java.util.Scanner;

public class A01E04 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double v1, v2, vf;
		
		System.out.print("V1 (km/h): ");
		v1 = sc.nextDouble();
		System.out.print("V2 (km/h): ");
		v2 = sc.nextDouble();
		
		vf = (v1+v2) / 2.0;
		System.out.print("Velocidade média final (km/h): " + vf);
		sc.close();
		
	}

}
