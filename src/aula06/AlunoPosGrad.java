package aula06;
import java.util.*;

public class AlunoPosGrad extends Aluno {
    private Scanner sc = new Scanner(System.in);
    private Professor orientador;

    public AlunoPosGrad(Professor orientador) {                   // construtores sem setters do orientador
        this.orientador = orientador;
    }
    public AlunoPosGrad(String datainscr, Professor orientador) {
        super(datainscr);
        this.orientador = orientador;
    }
    public AlunoPosGrad(String nome, int cc, String data, String datainscr, Professor orientador) {
        super(nome, cc, data, datainscr);
        this.orientador = orientador;
    }

    public AlunoPosGrad() {                                       // construtores com setters do orientador
        System.out.println("Nome do professor orientador: ");
        orientador.setNome(sc.next());
        System.out.println("Área de investigação do professor orientador: ");
        orientador.setArea(sc.next());
    }

    public AlunoPosGrad(String datainscr) {
        super(datainscr);
        System.out.println("Nome do professor orientador: ");
        orientador.setNome(sc.next());
        System.out.println("Área de investigação do professor orientador: ");
        orientador.setArea(sc.next());
    }

    public AlunoPosGrad(String nome, int cc, String data, String datainscr) {
        super(nome, cc, data, datainscr);
        System.out.println("Nome do professor orientador: ");
        orientador.setNome(sc.next());
        System.out.println("Área de investigação do professor orientador: ");
        orientador.setArea(sc.next());
    }

    public Professor getOrientador() {return orientador;}
    public void setOrientador(Professor orientador) {this.orientador = orientador;}

    @Override
    public String toString() {
        return "AlunoPosGrad{" + super.toString() + ", Orientador{nome= " + orientador.getNome() + ", área de investigação= " + orientador.getArea() + "}" + '}';
    }
}
