package aula06;
import java.util.*;

public class A06E03 {                                          // neste exercicio optei por criar apenas duas subclasses da classe Passageiro, por razoes de organizacao e menos trabalho,
    public static Scanner sc = new Scanner(System.in);         // (...) tendo implementado 3 classes poderia ter criado a interface das milhas mas nao achei necessario
    public static void main(String[] args) {
        ArrayList<Passageiro> registados = new ArrayList<Passageiro>(){};
        boolean control = true;
        while (control) {
            System.out.println("O que pretende fazer? (1= Registar novo passageiro; 2= Apresentar lista de passageiros inscritos; 3= terminar processo)");
            switch(sc.nextInt()) {
                case 1:
                    System.out.println("Em que classe pretende viajar? (1= Classe Turística; 2= Classe Executiva; 3= Primeira Classe)");
                    switch(sc.nextInt()) {
                        case 1:
                            SegundaClasse a = new SegundaClasse();
                            System.out.println("Deseja acumular milhas? (s/n)");
                            if (sc.next().equals("s") || sc.next().equals("S")) {
                                a.calcMilhasTuristica();
                            }
                            registados.add(a);
                            break;
                        case 2:
                            SegundaClasse b = new SegundaClasse();
                            System.out.println("Deseja acumular milhas? (s/n)");
                            if (sc.next().equals("s") || sc.next().equals("S")) {
                                b.calcMilhasExecutiva();
                            }
                            registados.add(b);
                            break;
                        case 3:
                            PrimeiraClasse c = new PrimeiraClasse();
                            System.out.println("Deseja acumular milhas? (s/n)");
                            if (sc.next().equals("s") || sc.next().equals("S")) {
                                c.calcMilhas();
                            }
                            registados.add(c);
                            break;
                        default:
                            System.out.println("Input Inválido.");
                            break;
                    } // end of creating passenger
                    break;
                case 2:
                    int id = 1;
                    for (Passageiro a: registados) {
                        if( a instanceof PrimeiraClasse) {   // instanceof permite saber em que nivel da heranca estamos
                            System.out.println("***" + id + ": " + a);
                            id++;

                        }
                    }
                    for (Passageiro a: registados) {
                        if( a instanceof SegundaClasse) {
                            System.out.println("   " + id + ": " + a);
                            id++;

                        }
                    }
                    break;
                case 3:
                    control = false;           // permite que o programa saia do loop
                    break;
                default:
                    System.out.println("Input Inválido.");
                    break;
            } // end of switch case
        } // end of loop
        System.out.println("Tenha um bom dia!");
    } // end of main
} // end of exercise
