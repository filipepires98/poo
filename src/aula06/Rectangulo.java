package aula06;

public class Rectangulo extends Figura {
    private int l, c, area, per;

    public void setL(int l) {this.l = l;}
    public void setC(int c) {this.c = c;}

    public int getL() {return l;}
    public int getC() {return c;}

    public Rectangulo(int x, int y, int l, int c) {
        super(x, y);
        this.l = l;
        this.c = c;
        area = l*c;
        per = 2*l + 2*c;
    }
    public Rectangulo(int l, int c) {
        super(0, 0);
        this.l = l;
        this.c = c;
        area = l*c;
        per = 2*l + 2*c;
    }
    public Rectangulo() {
        System.out.print("length = ");
        l = sc.nextInt();
        System.out.print("height = ");
        c = sc.nextInt();
        area = l*c;
        per = 2*l + 2*c;
    }

    @Override
    public String toString() {
        return " Rectangulo{" + super.toString() + "largura =" + l + "cm, altura =" + c + "cm, área = " + area + "cm2, perímetro = " + per + "cm" +'}';
    }
}
