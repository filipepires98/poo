package aula06;
import java.util.*;

public class Disciplina {
    private Scanner sc = new Scanner(System.in);
    private String disciplina;
    private Professor responsavel;
    private ArrayList<Object> inscritos = new ArrayList<Object>();

    public Disciplina() {                                                                        // input total do utilizador
        System.out.println("Nome da disciplina: ");
        disciplina = sc.nextLine();
        System.out.print("Nome do professor responsável: ");
        String npr = sc.nextLine();
        System.out.print("Área de investigação do professor responsável: ");
        String apr = sc.nextLine();
        responsavel = new Professor(npr, 0001, "01-01-1950", apr);   // cc e data sao irrelevantes

        Aluno a;
        System.out.println("Inscreva um aluno na disciplina.");
        System.out.print("Nome do aluno: ");
        String na = sc.nextLine();
        System.out.println("Data de nascimento do aluno: ");
        String da = sc.nextLine();
        System.out.println("Data de inscrição do aluno: ");
        String dia = sc.nextLine();
        System.out.print("CC do aluno: ");
        int cc = sc.nextInt();
        a = new Aluno(na, cc, da, dia);
        inscritos.add(a);
    }

    public Disciplina(String disciplina, Professor responsavel) {                                  // input parcial do utilizador
        this.disciplina = disciplina;
        this.responsavel = responsavel;

        boolean control = true;
        while (control) {
            Aluno a;
            System.out.println("Deseja inscrever alunos na disciplina? (s/n)");
            if ((sc.nextLine()).equals("s")) {
                System.out.println("Nome do aluno: ");
                String na = sc.nextLine();
                System.out.println("Data de nascimento do aluno: ");
                String da = sc.nextLine();
                System.out.println("Data de inscrição do aluno: ");
                String dia = sc.nextLine();
                System.out.println("CC do aluno: ");
                int cc = sc.nextInt();
                a = new Aluno(na, cc, da, dia);
                inscritos.add(a);
            } else {
                control = false;
            }
        }
    }

    public Disciplina(String disciplina, Professor responsavel, ArrayList<Object> inscritos) {             // input automático
        this.disciplina = disciplina;
        this.responsavel = responsavel;
        this.inscritos = inscritos;
    }

    public String getDisciplina() {return disciplina;}
    public void setDisciplina(String disciplina) {this.disciplina = disciplina;}
    public Professor getResponsavel() {return responsavel;}
    public void setResponsavel(Professor responsavel) {this.responsavel = responsavel;}
    public ArrayList<Object> getInscritos() {return inscritos;}
    public void setInscritos(ArrayList<Object> inscritos) {this.inscritos = inscritos;}

    public void adicionaralunos() {
        boolean control = true;
        while (control) {
            Aluno a;
            System.out.println("Deseja inscrever alunos na disciplina? (s/n)");
            if ((sc.next()).equals("s")) {
                System.out.print("Nome do aluno: ");
                String na = sc.next();
                System.out.print("Data de nascimento do aluno: ");
                String da = sc.next();
                System.out.print("Data de inscrição do aluno: ");
                String dia = sc.next();
                System.out.print("CC do aluno: ");
                int cc = sc.nextInt();
                a = new Aluno(na, cc, da, dia);
                inscritos.add(a);
            } else {
                control = false;
            }
        }
    }


    @Override
    public String toString() {
        return "Disciplina{" + disciplina + '\'' + ", Responsável= {nome= " + responsavel.getNome() + ", área de investigação= " + responsavel.getArea() + ", inscritos= " + inscritos + '}';
    }
}
