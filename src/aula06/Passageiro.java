package aula06;
import java.util.*;

public abstract class Passageiro {
    Scanner sc = new Scanner(System.in);
    String[] destinos = new String[]{"Madrid","Madeira","Paris","Londres","Roma","Amesterdão"};
    int[] distancias = new int[]{320,900,920,990,1150,1150};
    private String nome, datanasc, sexo;
    private int viagens;

    public Passageiro(String nome, String datanasc, String sexo, int viagens) {
        this.nome = nome;
        this.datanasc = datanasc;
        this.sexo = sexo;
        this.viagens = viagens;
    }

    public Passageiro() {
        System.out.println("Nome e apelido do Passageiro: ");
        nome = sc.nextLine();
        System.out.println("Data de nascimento: ");
        datanasc = sc.nextLine();
        System.out.println("Sexo: ");
        sexo = sc.nextLine();
        System.out.println("Número de viagens efetuadas em 2017: ");
        viagens = sc.nextInt();
    }

    public String getNome() {return nome;}
    public void setNome(String nome) {this.nome = nome;}
    public String getDatanasc() {return datanasc;}
    public void setDatanasc(String datanasc) {this.datanasc = datanasc;}
    public String getSexo() {return sexo;}
    public void setSexo(String sexo) {this.sexo = sexo;}
    public int getViagens() {return viagens;}
    public void setViagens(int viagens) {this.viagens = viagens;}

    @Override
    public String toString() {
        return "Passageiro{" + "nome= '" + nome + '\'' + ", data de nascimento= '" + datanasc + '\'' + ", sexo= '" + sexo + '\'' + ", número de viagens= " + viagens + '}';
    }
}
