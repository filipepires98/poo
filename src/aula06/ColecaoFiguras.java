package aula06;
import java.util.*;

public class ColecaoFiguras {
    private static Scanner sc = new Scanner(System.in);
    private double maxArea;
    private ArrayList<Object> colecao = new ArrayList<Object>();

    public ColecaoFiguras() {
        System.out.println("Input maximum area for the colection figures: ");
        maxArea = sc.nextInt();
    }
    public ColecaoFiguras(double maxArea) {
        this.maxArea = maxArea;
    }

    public boolean addFigura(Figura f) {
        if (colecao.contains(f)) {
            return false;
        }
        if (f instanceof Circulo) {                     // INSTANCE OF - importante para saber natureza do objeto a nivel de herancas
            int radium = ((Circulo) f).getRadium();
            double areacirculo = 3.14 * Math.pow(radium, 2);
            if (areacirculo <= maxArea) {
                colecao.add(f);
                return true;
            }
        } else if (f instanceof Quadrado) {
            int c = ((Quadrado) f).getC();
            double areaquadrado = c*c;
            if (areaquadrado <= maxArea) {
                colecao.add(f);
                return true;
            }
        } else if (f instanceof Rectangulo) {
            int c = ((Rectangulo) f).getC();
            int l = ((Rectangulo) f).getL();
            double arearectangulo = c*l;
            if (arearectangulo <= maxArea) {
                colecao.add(f);
                return true;
            }
        } else {
            System.out.println("Figura Inválida.");
        }
        return false;
    }

    public boolean delFigura(Figura f) {
        if (colecao.contains(f)) {
            colecao.remove(f);
            return true;
        } else {
            return false;
        }
    }

    public double areaTotal() {
        double areatotal = 0;
        for (Object f: colecao) {
            if (f instanceof Circulo) {                     // INSTANCE OF - importante para saber natureza do objeto a nivel de herancas
                int radium = ((Circulo) f).getRadium();
                double areacirculo = 3.14 * Math.pow(radium, 2);
                areatotal = areatotal + areacirculo;

            } else if (f instanceof Quadrado) {
                int c = ((Quadrado) f).getC();
                double areaquadrado = c*c;
                areatotal = areatotal + areaquadrado;

            } else if (f instanceof Rectangulo) {
                int c = ((Rectangulo) f).getC();
                int l = ((Rectangulo) f).getL();
                double arearectangulo = c * l;
                areatotal = areatotal + arearectangulo;

            }
        }
        return areatotal;
    }

    public boolean exists(Figura f) {
        if (colecao.contains(f)) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "ColecaoFiguras{" + "maxArea= " + maxArea + "cm2, coleção= " + colecao + '}';
    }

    public Figura[] getFiguras() {
        int l = colecao.size();
        Figura[] lista = new Figura[l];
        int i = 0;
        for (Object a: colecao) {
            lista[i] = (Figura)a;
            i++;
        }
        return lista;
    }

    public String[] getCentros() {
        int l = colecao.size();
        String[] lista = new String[l];
        int i = 0;
        for (Object a: colecao) {
            a = (Figura)a;
            int x = ((Figura) a).getX();
            int y = ((Figura) a).getY();
            lista[i] = "x: " + x + ", y: " + y;
            i++;
        }
        return lista;
    }
}