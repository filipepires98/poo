package aula06;

public class Quadrado extends Figura {
    private int c, area, per;

    public void setC(int l) {this.c = c;}
    public int getC() {return c;}

    public Quadrado(int x, int y, int c) {
        super(x, y);
        this.c = c;
        area = c*c;
        per = 4*c;
    }
    public Quadrado(int c) {
        super(0, 0);
        this.c = c;
        area = c*c;
        per = 4*c;
    }
    public Quadrado() {
        System.out.print("length = ");
        c = sc.nextInt();
        area = c*c;
        per = 4*c;
    }

    @Override
    public String toString() {
        return " Quadrado{" + super.toString() + ", comprimento lateral = " + c + "cm, área = " + area + "cm2, perímetro = " + per + "cm" +'}';
    }
}
