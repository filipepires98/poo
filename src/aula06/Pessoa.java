package aula06;

public class Pessoa {
    private String nome;
    private int cc;
    private String data;

    public int getCc() {return cc;}
    public String getNome() {return nome;}
    public String getData() {return data;}

    public void setCc(int cc) {this.cc = cc;}
    public void setNome(String nome) {this.nome = nome;}
    public void setData(String data) {this.data = data;}

    public Pessoa() {                                          // pre-definido
        this.nome = "Filipe Pires";
        this.cc = 13831291;
        this.data = "25-06-1998";
    }
    public Pessoa(String nome, int cc, String data) {
        this.nome = nome;
        this.cc = cc;
        this.data = data;
    }

    @Override
    public String toString() {
        return "Pessoa{" + "nome= '" + nome + '\'' + ", cc= " + cc + ", data de nascimento= '" + data + '\'' + '}';
    }
}
