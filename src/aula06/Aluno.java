package aula06;

public class Aluno extends Pessoa {
    private static int count = 100;
    private final int nmec;
    private String datainscr;

    public int getNmec() {return nmec;}
    public String getDatainscr() {return datainscr;}
    public void setDatainscr(String datainscr) {this.datainscr = datainscr;}

    public Aluno() {
        nmec = count;
        count++;
        datainscr = "01-01-2000";
    }

    public Aluno(String datainscr) {
        nmec = count;
        count++;
        this.datainscr = datainscr;
    }

    public Aluno(String nome, int cc, String data, String datainscr) {
        super(nome, cc, data);
        nmec = count;
        count++;
        this.datainscr = datainscr;
    }

    @Override
    public String toString() {
        return "Aluno{" + super.toString() + ", nmec= " + nmec + ", data de inscrição= '" + datainscr + '\'' + '}';
    }
}
