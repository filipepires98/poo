package aula06;

public class Professor extends Pessoa {
    private String area;

    public Professor() {                                      // pre-definido
        this.area = "Sem área de investigação";
    }

    public Professor(String area) {
        this.area = area;
    }

    public Professor(String nome, int cc, String data, String area) {
        super(nome, cc, data);
        this.area = area;
    }

    public String getArea() {return area;}
    public void setArea(String area) {this.area = area;}


    @Override
    public String toString() {
        return "Professor{" + super.getNome().toString() + ", área de investigação= '" + area + '\'' + '}';
    }
}
