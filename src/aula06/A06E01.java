package aula06;
import java.util.*;

public class A06E01 {
    public Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        Professor a = new Professor("Sousa Pinto", 12342515, "12-12-1945", "Tecnologias Web");
        System.out.println(a);

        Pessoa b = new AlunoPosGrad("Filipe Pires", 13831291, "25-06-1998", "21-07-2016", a);
        System.out.println(b);

        Disciplina poo = new Disciplina();
        poo.adicionaralunos();
        System.out.println(poo);
    }
}
