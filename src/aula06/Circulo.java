package aula06;

public class Circulo extends Figura {
    private int radium;
    private double area, per;

    public void setRadium(int radium) {this.radium = radium;}
    public int getRadium() {return radium;}

    public Circulo(int x, int y, int radium) {
        super(x, y);
        this.radium = radium;
        area = 3.14*Math.pow(radium,2);
        per = 6.28*radium;
    }
    public Circulo(int radium) {
        super(0, 0);
        this.radium = radium;
        area = 3.14*Math.pow(radium,2);
        per = 6.28*radium;
    }
    public Circulo() {
        System.out.print("radium = ");
        radium = sc.nextInt();
        area = 3.14*Math.pow(radium,2);
        per = 6.28*radium;
    }

    @Override
    public String toString() {
        return " Círculo{" + super.toString() + ", raio = " + radium + "cm" + ", área = " + Math.round(area) + "cm2" + ", perímetro = " + Math.round(per) + "cm" + '}';
    }
}
