package aula06;

public class SegundaClasse extends Passageiro {
    private double milhas = 0;
    private String destino;

    public SegundaClasse(String nome, String datanasc, String sexo, int viagens, int milhas) {
        super(nome, datanasc, sexo, viagens);
        this.milhas = milhas;
    }

    public SegundaClasse() {

        System.out.print("\n{");
        for (String a: destinos) {
            if (a.equals("Amesterdão")) {
                System.out.print(a + "}\n");
            } else {
                System.out.print(a + ", ");
            }
        }
        System.out.println("Destino? (escreva corretamente)");
        destino = sc.next();
    }

    public void calcMilhasTuristica() {
        int i = 0;
        for (String d: destinos) {
            if(d.equals(destino)) {
                break;
            }
            i++;
        }
        int m = distancias[i];

        milhas = m;
    }
    public void calcMilhasExecutiva() {
        int i = 0;
        for (String d: destinos) {
            if(d.equals(destino)) {
                break;
            }
            i++;
        }
        int m = distancias[i];

        milhas = 2*m;
    }

    public double getMilhas() {return milhas;}
    public String[] getDestinos() {return destinos;}

    @Override
    public String toString() {
        if (milhas == 0) {
            return "{" + super.toString() + ", destino= " + destino + '}';
        } else {
            return "{" + super.toString() + ", destino= " + destino + ", número de milhas por utilizar= " + milhas + '}';
        }
    }
}
