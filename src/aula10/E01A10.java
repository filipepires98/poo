package aula10;
import java.util.*;
import java.io.*;

public class E01A10 {
    public static void main(String[] args) {
        Scanner input = null;
        PrintWriter pw = null;

        File words = new File("words_E01A10.txt");
        File newfile = new File("fileCreated_E01A10.txt");
        try {
            pw = new PrintWriter(newfile);
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
            System.out.println("Ficheiro nao existe (via exceção)!");
            System.exit(0);
        }

        ArrayList<String> arraylist = new ArrayList<>();          // estrutura para alineas a), d), e)
        ArrayList<String> arrayMoreThan2 = new ArrayList<>();     //     =      =   alinea b)
        ArrayList<String> arrayOfS = new ArrayList<>();           //     =      =      =   c)

        try {
            input = new Scanner(words);
            System.out.print("a) " + input.nextLine() + " \n");
            while (input.hasNext()) {
                String word = input.next();
// a)
                System.out.println(word);
                if (!arraylist.contains(word)) {
                    arraylist.add(word);
                }
// b)
                if (word.length() >= 2) {
                    pw.print(word + " \n");                  // nao e pedido no exercicio, mas decidi treinar escrever num novo ficheiro todas as palavras com mais de 2 caracteres
                    if (!arrayMoreThan2.contains(word)) {
                        arrayMoreThan2.add(word);
                    }
                }
// c)
                if (word.substring(word.length()-1).equals("s")) {
                    if(!arrayOfS.contains(word)) {
                        arrayOfS.add(word);
                    }
                }
// d)
                if (!word.chars().allMatch(Character::isLetter)) {
                    arraylist.remove(word);
                }

            }
        } catch (FileNotFoundException e) {
            System.out.println("Ficheiro não existente.");
        } finally {
            System.out.println("\nNOTA: Tudo o que se encontra neste bloco é sempre executado.");    // a nao ser que o programa termine antes
        }

// c)
        System.out.print("\nc) \n");
        for (String w: arrayOfS) {
            System.out.print(w + " ");
        }
// e)
        System.out.print("\ne) \n");
        for (String w: arraylist) {
            System.out.print(w + " ");
        }

        pw.close();
        input.close();
    }
}
