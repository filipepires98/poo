package aula10;
import java.util.*;

class ValueComparator implements Comparator<String> {
    Map<String, Double> base;

    public ValueComparator(Map<String, Double> base) {
        this.base = base;
    }

    @Override
    public int compare(String a, String b) {               // esta em ordem crescente, para fazer sorte em decrescente trocar returns pra -1 e 1
        int res = base.get(a).compareTo(base.get(b));
        return res != 0 ? res : 1;
        // returning 0 would merge keys
    }
}