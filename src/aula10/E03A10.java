package aula10;
import java.util.*;
import java.io.*;

public class E03A10 {
    public static void main(String[] args) {
        System.out.println("----------------------- TABELA DE INFORMAÇÕES SOBRE PRÓXIMOS VOOS: ------------------------\n");

        // a)
        // Arrays necessários
        ArrayList<String> hora = new ArrayList<>();                 // horas dos voos
        ArrayList<String> voo = new ArrayList<>();                  // numeros dos voos
        ArrayList<String> companhia = new ArrayList<>();            // lista de todas as companhias
        ArrayList<String> sigla_companhia = new ArrayList<>();      // lista das siglas de cada companhia
        ArrayList<String> companhia_voo = new ArrayList<>();        // lista de companhias de acordo com cada voo
        ArrayList<String> origem = new ArrayList<>();               // origens
        ArrayList<String> atraso = new ArrayList<>();               // tempo de atraso dos voos
        ArrayList<String> observacao = new ArrayList<>();           // tempos previstos tendo em conta atraso


        // Ficheiros necessários
        File voos = new File("voos.txt");
        File companhias = new File("companhias.txt");

        // FICHEIRO VOOS (horas, voos, origens, atrasos)
        try {
            Scanner scv = new Scanner(voos);
            int index = 0;
            String line;

            while (scv.hasNextLine()) {
                index++;
                line = scv.nextLine();
                String[] parts = line.split("\\t");

                if (parts.length < 4) {
                    atraso.add("");
                }

                for (int i=0; i < parts.length; i++) {
                    switch(i) {
                        case 0:
                            hora.add(parts[i]);
                            break;
                        case 1:
                            voo.add(parts[i]);
                            break;
                        case 2:
                            origem.add(parts[i]);
                            break;
                        case 3:
                            atraso.add(parts[i]);
                            break;
                    }
                }
            }
            //System.out.println(hora);
            //System.out.println(voo);
            //System.out.println(origem);
            //System.out.println(atraso);

        } catch (FileNotFoundException e) {
            //e.printStackTrace();                                            // faz print da razao do erro
            System.out.println("Ficheiro 'voos.txt' nao existe!");
            System.exit(0);
        }

        // FICHEIRO COMPANHIAS (Siglas, Companhias)
        try {
            Scanner scc = new Scanner(companhias);
            int index = 0;
            String line;

            while (scc.hasNextLine()) {
                line = scc.nextLine();
                String[] parts = line.split("\\t");

                for (int i=0; i < parts.length; i++) {
                    switch(i) {
                        case 0:
                            sigla_companhia.add(parts[i]);
                            break;
                        case 1:
                            companhia.add(parts[i]);
                            break;
                    }
                }
            }
            //System.out.println(companhia);
            //System.out.println(sigla_companhia);

        } catch (FileNotFoundException e) {
            //e.printStackTrace();
            System.out.println("Ficheiro 'voos.txt' nao existe!");
            System.exit(0);
        }

        // Calcular previsoes para atrasos
        observacao.add("Obs");
        for (int i=1; i < hora.size(); i++) {
            if (!atraso.get(i).equals("")) {
                int horas = Integer.parseInt(hora.get(i).substring(0,2));
                int minutos = Integer.parseInt(hora.get(i).substring(3,5)) + Integer.parseInt(atraso.get(i).substring(3,5));
                if (minutos >= 60) {
                    horas = horas +1;
                    minutos = minutos - 60;
                }

                String hh;
                String mm;
                if (horas < 10) {
                    hh = "0" + horas;
                } else {
                    hh = "" + horas;
                }
                if (minutos < 10) {
                    mm = "0" + minutos;
                } else {
                    mm = "" + minutos;
                }

                observacao.add("Previsto: " + hh + ":" + mm);
            } else {
                observacao.add("");
            }
        }


        // Verificar primeiras 2 letras de cada voo para saber a companhia responsavel
        companhia_voo.add("Companhia");
        for (int i=1; i < voo.size(); i++) {
            String sigla_voo = voo.get(i).substring(0,2);
            String companhia_responsavel = "";

            for (int ii = 1; ii < sigla_companhia.size(); ii++) {
                if (sigla_voo.equals(sigla_companhia)) {                                                   // METODO .EQUALS() NAO FUNCIONA POR ALGUMA RAZAO
                    companhia_responsavel = companhia.get(ii);
                }
            }

            if (sigla_voo.equals("A5")) {                                              // TIVE DE IR BUSCAR AS COMPANHIAS MANUALMENTE
                companhia_responsavel = companhia.get(1);
            } else if (sigla_voo.equals("AE")) {
                companhia_responsavel = companhia.get(2);
            } else if (sigla_voo.equals("DT")) {
                companhia_responsavel = companhia.get(3);
            } else if (sigla_voo.equals("EV")) {
                companhia_responsavel = companhia.get(4);
            } else if (sigla_voo.equals("EZ")) {
                companhia_responsavel = companhia.get(5);
            } else if (sigla_voo.equals("FR")) {
                companhia_responsavel = companhia.get(6);
            } else if (sigla_voo.equals("IB")) {
                companhia_responsavel = companhia.get(7);
            } else if (sigla_voo.equals("LG")) {
                companhia_responsavel = companhia.get(8);
            } else if (sigla_voo.equals("LH")) {
                companhia_responsavel = companhia.get(9);
            } else if (sigla_voo.equals("LX")) {
                companhia_responsavel = companhia.get(10);
            } else if (sigla_voo.equals("S4")) {
                companhia_responsavel = companhia.get(11);
            } else if (sigla_voo.equals("SN")) {
                companhia_responsavel = companhia.get(12);
            } else if (sigla_voo.equals("TK")) {
                companhia_responsavel = companhia.get(13);
            } else if (sigla_voo.equals("TO")) {
                companhia_responsavel = companhia.get(14);
            } else {
                companhia_responsavel = companhia.get(15);
            }

                companhia_voo.add(companhia_responsavel);
        }

        // Imprimir tabela de informaçoes
        for (int i=0; i < hora.size(); i++) {
            System.out.printf("%-8s%-10s%-25s%-25s%-8s%-15s\n", hora.get(i), voo.get(i), companhia_voo.get(i), origem.get(i), atraso.get(i), observacao.get(i));
        }

        System.out.println("-------------------------------------------------------------------------------------------\n");

        // b) - Salvar tabela num ficheiro externo
        try {
            FileWriter InfoPublico = new FileWriter("InfoPublico.txt");
            for (int i=0; i < hora.size(); i++) {
                InfoPublico.write(hora.get(i) + "\t" + voo.get(i) + "\t" + companhia_voo.get(i) + "\t" + origem.get(i) + "\t" + atraso.get(i) + "\t" + observacao.get(i));
            }

            InfoPublico.close();
        } catch (IOException e) {          // IOException significa que ha um erro de leitura de ficheiros, nao especifica o erro em concreto (como o FileNotFoundExepcion faz)
            System.out.println("Problemas na criação do ficheiro InfoPublico.txt!");
            System.exit(0);
        }

        /*
        // e) alinea b) mas armazenando dados num ficheiro binario; Construa também um método para ler o ficheiro e apresentar a tabela no ecrã. Use como base a classe RandomAccessFile. RandomAccessFile file = new RandomAccessFile("Infopublico.bin", "rw");
        try {
            FileWriter IPBinario = new FileWriter("InfoPublicoBinario.txt");
            for (int i=0; i < hora.size(); i++) {
                IPBinario.write(hora.get(i) + "; " + voo.get(i) + "; " + companhia_voo.get(i) + "; " + origem.get(i) + "; " + atraso.get(i) + "; " + observacao.get(i));
            }

            IPBinario.close();
        } catch (IOException e) {          // IOException significa que ha um erro de leitura de ficheiros, nao especifica o erro em concreto (como o FileNotFoundExepcion faz)
            System.out.println("Problemas na criação do ficheiro InfoPublicoBinario.txt!");
            System.exit(0);
        }
        */

        // c) - Calcular a média dos atrasos por companhia e apresentar no ecrã uma tabela ordenada por ordem crescente de atraso médio
        // maps required for c)
        Map<String, Double> mediaAtrasos = new HashMap<>();
        Map<String, Integer> totalAtrasos = new HashMap<>();
        Map<String, Integer> numVoos = new HashMap<>();

        // preparing all 3 maps
        for (String a: companhia) {
            if (!a.equals("Companhia")) {
                mediaAtrasos.put(a,0.0);
                totalAtrasos.put(a,0);
                numVoos.put(a,0);
            }
        }
        /*
        System.out.println(companhia_voo);
        System.out.println(atraso);
        System.out.println(totalAtrasos);
        System.out.println(companhia_voo.size());
        System.out.println(atraso.size());
        System.out.println(totalAtrasos.size());
        */

        for (int i = 1; i < companhia_voo.size(); i++) {
            // setting totalAtrasos map <companhia, total de minutos de atraso>
            if (!atraso.get(i).equals("")) {
                //System.out.print(atraso.get(i) + ", ");
                int min = Integer.parseInt(atraso.get(i).substring(3,5));
                //System.out.print(min + ", ");
                int t = totalAtrasos.get(companhia_voo.get(i)) + min;
                //System.out.print(t + ", ");
                totalAtrasos.replace(companhia_voo.get(i),t);
            }

            // setting numVoos map <companhia, numero total de voos>
            int nv = numVoos.get(companhia_voo.get(i)) + 1;
            numVoos.replace(companhia_voo.get(i),nv);
        }
        // setting mediaAtrasos map <companhia, media de minutos de atraso>
        for (String key: mediaAtrasos.keySet()) {
            double media = totalAtrasos.get(key) / numVoos.get(key);
            mediaAtrasos.replace(key, media);
        }

        /*
        System.out.print("\n");
        System.out.println(totalAtrasos);
        System.out.print("\n");
        System.out.println(numVoos);
        System.out.print("\n");
        System.out.println(mediaAtrasos);
        */

        // sorting voos por media de tempo de atraso
        ValueComparator vc1 = new ValueComparator(mediaAtrasos);
        TreeMap<String, Double> sorted_mediaAtrasos = new TreeMap<>(vc1);
        sorted_mediaAtrasos.putAll(mediaAtrasos);
        System.out.println("mediaAtraso: " + mediaAtrasos);
        sorted_mediaAtrasos.put("Ryanair", 50.0);
        System.out.println("sorted_mediaAtrasos: " + sorted_mediaAtrasos);

        System.out.println(sorted_mediaAtrasos.get("Ryanair"));               //esta a imprimir null????
        for (Map.Entry<String, Double> e: sorted_mediaAtrasos.entrySet()) {
            System.out.printf("%-20s" + e.getValue() + "\n", e.getKey());
        }


        // d) cidades.txt tabela de chegadas de cada origem ordenada de forma decrescente
        // tive de fazer sorting manual pois quando fiz esta alinea, o sorting da alinea anterior estava a imprimir os inteiros "null" e nao queria repetir isso aqui


        // collections necessarias para alinea d)
        Map<String, Integer> voosCidade = new HashMap<>();
        ArrayList<String> sorted_voosCidade = new ArrayList<>();
        ArrayList<Integer> sorted_voosCidadeN = new ArrayList<>();
        Map<String, Integer> controler = new HashMap<>();

        // setting voosCidade <cidade, nº de chegadas>
        for (String cidade: origem) {
            if (!voosCidade.keySet().contains(cidade)) {
                voosCidade.put(cidade,1);
            } else {
                int n = voosCidade.get(cidade) +1;
                voosCidade.replace(cidade,n);
            }
        }

        // creating a map identical to voosCidade for control; sorting voosCidade by adding cidades (already sorted) to array1, number of arrivals (also sorted) to array2 and removing elements from control map
        controler.putAll(voosCidade);
        while (sorted_voosCidade.size() < voosCidade.size()) {
            int max = 0;
            String top = "";
            for (String cidade: controler.keySet()) {
                int n = controler.get(cidade);
                if (n > max) {
                    max = n;
                    top = cidade;
                }
            }
            if (sorted_voosCidade.contains(top)) {
                System.out.println("erro");          // so para caso haja algum problema a meio do codigo
            } else {
                sorted_voosCidade.add(top);
                sorted_voosCidadeN.add(max);
                controler.remove(top);
            }
        }
        /*
        System.out.println(voosCidade);
        for (int i = 0; i < sorted_voosCidade.size(); i++) {
            System.out.print(sorted_voosCidade.get(i) + "=" + sorted_voosCidadeN.get(i) + ", ");
        }
        */

        // salvar tabela num ficheiro externo
        try {
            FileWriter cidadesFile = new FileWriter("cidades.txt");
            for (int i=0; i < sorted_voosCidade.size(); i++) {
                cidadesFile.write(sorted_voosCidade.get(i) + "\t" + sorted_voosCidadeN.get(i));
            }

            cidadesFile.close();
        } catch (IOException e) {
            System.out.println("Problemas na criação do ficheiro cidades.txt!");
            System.exit(0);
        }

        // e) alinea b) mas armazenando dados num ficheiro binario; Construa também um método para ler o ficheiro e apresentar a tabela no ecrã. Use como base a classe RandomAccessFile. RandomAccessFile file = new RandomAccessFile("Infopublico.bin", "rw");

         // FAZER !!! Como ???
    }
}
