package aula10;
import java.util.*;
import java.io.*;

public class E02A10 {
    public static void main(String[] args) {
        Scanner sc = null;
        File f1 = new File("paresPalavras_E02A10.txt");
        ArrayList<String> palavras = new ArrayList<>();
        Map<String, Integer> ocorrencias = new HashMap<>();
        Map<String, ArrayList> ocor_por_palavr = new HashMap<>();

        try {
            char[] chars = {'\t','\n','.',',',':','"',';','?','!','-','*','{','}','+','&','/','(',')','[',']','”','“'};
            String[] forbiddenchars = {"\t","\n",".",",",":",";","?","!","-","*","{","}","[","]","(",")","&","+","/"," "};

            sc = new Scanner(f1);
            while (sc.hasNext()) {                                                       // enquanto ficheiro contem palavras
                String word = sc.next();
                String[] parts = word.split("[\t\n.,:';?!-*+{}()/&\"\']");
                for (String a: parts) {
                    palavras.add(a);
                }
            }
        } catch (FileNotFoundException e) {                                   // exceções
            //e.printStackTrace();
            System.out.println("Ficheiro nao existe (via exceção)!");
            System.exit(0);
        }

        for (int i = 0; i < palavras.size(); i++) {
            String word1;
            String word2;
            if (i == palavras.size()-1) {
                word1 = palavras.get(i);
                word2 = palavras.get(0);
            } else {
                word1 = palavras.get(i);
                word2 = palavras.get(i+1);
            }

            String par = "{" + word1 + ", " + word2 + "}";
            if (!ocorrencias.containsKey(par)) {
                ocorrencias.put(par,1);
            } else {
                ocorrencias.replace(par,ocorrencias.get(par)+1);
            }
        }
        System.out.print("Lista de Palavras:                " + palavras + "\n");
       // System.out.print("\n");
        System.out.print("Lista de Ocorrências de cada Par: " + ocorrencias + "\n");
    }
}