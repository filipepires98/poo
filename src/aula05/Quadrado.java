package aula05;

public class Quadrado extends Figura implements java.io.Serializable {           // implements é apenas para testes (package exercises), nao altera nada deste programa
    private int c, area, per;

    public void setC(int l) {this.c = c;}
    public int getC() {return c;}

    public Quadrado(int x, int y, int c) {
        super(x, y);
        this.c = c;
        area = c*c;
        per = 4*c;
    }

    public Quadrado() {
        System.out.print("length = ");
        c = sc.nextInt();
        area = c*c;
        per = 4*c;
    }

    @Override
    public String toString() {
        return " Quadrado{" + super.toString() + ", comprimento lateral = " + c + "cm, área = " + area + "cm2, perímetro = " + per + "cm" +'}';
    }
}
