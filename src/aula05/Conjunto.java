package aula05;
import java.util.*;

public class Conjunto {
    Scanner sc = new Scanner(System.in);
    private ArrayList<Integer> array = new ArrayList<Integer>();

    public Conjunto() {
        this.array = array;
        /*
        int num;
        System.out.print("Adicione elementos ao conjunto (Insira 'stop' quando quiser terminar):\n- ");
        String control = sc.next();

        while (true) {
            if (control.equals("stop")) {
                break;
            }
            num = Integer.valueOf(control);             // converter string do numero para integer
            if (!(array.contains(num))) {
                array.add(num);
            } else {
                System.out.println("Número ja pertence ao conjunto.");
            }
            System.out.print("- ");
            control = sc.next();
        }
        */
    }

    public Conjunto(ArrayList<Integer> array) {
        this.array = array;
    }

    public ArrayList<Integer> getArray() {return array;}
    public void setArray(ArrayList<Integer> array) {this.array = array;}

    public void insert(int x) {
        if (!(array.contains(x))) {
            array.add(x);
        }
    }

    public boolean contains(int y) {
        if (array.contains(y)) {
            return true;
        } else {
            return false;
        }
    }

    public void remove(Object z) {
        if (array.contains(z)) {
            array.remove(z);
        }
    }

    public void empty() {
        array.clear();
    }

    @Override
    public String toString() {
        return "Conjunto{" + " array = " + array + '}';
    }

    public int size() {
        int size = array.size();
        return size;
    }

    public ArrayList<Integer> unir(Conjunto x) {
        ArrayList<Integer> c = new ArrayList<Integer>();
        ArrayList<Integer> c2 = x.getArray();
        for (int a : array) {
            c.add(a);
        }
        for (int b : c2) {
            if (!(c.contains(b))) {
                c.add(b);
            }
        }
        return c;
    }

    public ArrayList<Integer> interset(Conjunto x) {
        ArrayList<Integer> c = new ArrayList<Integer>();
        ArrayList<Integer> c2 = x.getArray();
        for (int a : array) {
            if (c2.contains(a)) {
                if (!(c.contains(a))) {
                    c.add(a);
                }
            }
        }
        return c;
    }

    public ArrayList<Integer> subtrair(Conjunto x) {
        ArrayList<Integer> c = new ArrayList<Integer>();
        ArrayList<Integer> c2 = x.getArray();
        for (int a : array) {
            if (!(c2.contains(a))) {
                if (!(c.contains(a))) {
                    c.add(a);
                }
            }
        }
        for (int b : c2) {
            if (!(array.contains(b))) {
                if (!(c.contains(b))) {
                    c.add(b);
                }
            }
        }
        return c;
    }
}
