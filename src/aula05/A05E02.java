package aula05;
import java.util.*;

public class A05E02 {                  // n tenho class ponto pq achei irrelevante, os unicos atributos da class figura seriam os msms q os da class ponto
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("This is a geometrical figure creator program. Please input the name of the figure correctly.\nIf you type nothing and hit ENTER, the process will finish.");
        System.out.println("Which figure (circle/rectangle/square) would you like to work on?");
        String f1 = sc.nextLine();

        while (f1.equals("") == false) {
            String[] figarray = {"circle","rectangle","square"};           // converting choice to number for convenience
            int intfig1=0;
            for (String a: figarray) {
                if (a.equals(f1)) {break;}
                intfig1++;
            }

            int xa, xb, ya, yb, rr, la, lb, ca, cb;        // all values needed
            double dd;
            switch (intfig1) {                                              // use cases
                case 0:
                    System.out.println("Write circle A's center coordenates and it's radium: ");
                    Circulo a = new Circulo();
                    System.out.println("Write circle B's center coordenates and it's radium: ");
                    Circulo b = new Circulo();
                    System.out.println("A{" + a + "}");
                    System.out.println("B{" + b + "}");

                    xa = a.getX(); xb = b.getX();        // intersections
                    ya = a.getY(); yb = b.getY();
                    rr = a.getRadium() + b.getRadium();
                    dd = Math.sqrt(Math.pow((xb-xa),2) + Math.pow((yb-ya),2));

                    if (dd > rr) {System.out.println("Circles do not intersect.\n");}
                    else if (dd == rr) {System.out.println("Circles intersect in one point.\n");}
                    else {System.out.println("Circles intersect in more than one point.\n");}
                    break;
                case 1:
                    System.out.println("Write rectangle A's center coordenates, it's length and height: ");
                    Rectangulo c = new Rectangulo();
                    System.out.println("Write rectangle B's center coordenates, it's lateral length and height: ");
                    Rectangulo d = new Rectangulo();
                    System.out.println("A{" + c + "}");
                    System.out.println("B{" + d + "}");

                    xa = c.getX(); xb = d.getX();        // intersections
                    ya = c.getY(); yb = d.getY();
                    la = c.getL(); lb = d.getL();
                    ca = c.getC(); cb = d.getC();
                    if (( xb-(lb/2) > xa + (la/2)) || (yb-(cb/2) > ya+(ca/2)) || (xa-(la/2) > xb+(lb/2)) || (ya-(ca/2) > yb+(cb/2))) {
                        System.out.println("Rectangles do not intersect.");
                    } else {System.out.println("Rectangles intersect.");}
                    break;
                case 2:
                    System.out.println("Write square A's center coordenates and it's lateral length: ");
                    Quadrado e = new Quadrado();
                    System.out.println("Write square B's center coordenates and it's lateral length: ");
                    Quadrado f = new Quadrado();
                    System.out.println("A{" + e + "}");
                    System.out.println("B{" + f + "}");

                    xa = e.getX(); xb = f.getX();        // intersections
                    ya = e.getY(); yb = f.getY();
                    ca = e.getC(); cb = f.getC();
                    if (( xb-(cb/2) > xa + (ca/2)) || (yb-(cb/2) > ya+(ca/2)) || (xa-(ca/2) > xb+(cb/2)) || (ya-(ca/2) > yb+(cb/2))) {
                        System.out.println("Squares do not intersect.");
                    } else {System.out.println("Squares intersect.");}
                    break;
                default:
                    System.out.println("Invalid Input.");
                    break;
            } // end of switch case

            //f1 = "";
            System.out.println("Would you like to compare new figures? (y/n) ");      // if else pra determinar se programa termina
            if (sc.nextLine().equals("y")) {
                System.out.println("Which figure (circle/rectangle/square) would you like to work on? (it's important that you write the name correctly)");
                f1 = sc.nextLine();
            } else {f1 = "";}
        } // end of while
    } // end of main
}
