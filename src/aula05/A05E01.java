package aula05;
import aula01.Data;   // class Data no package da aula01
import java.util.*;

public class A05E01 {
    public static Scanner sc = new Scanner(System.in);
    public static void main(String[] args) {

        /*
        System.out.print("Nome:");
        String nome = sc.nextLine();
        System.out.print("CC:");
        int cc = sc.nextInt();
        Pessoa a = new Pessoa(nome, cc, Data.DataSet());
        */
        // Pessoa a = new Pessoa();
        Pessoa a = new Pessoa("Filipe",13831291, "25-06-1998");
        System.out.println(a);

        // Pessoa b = new Aluno(nome, cc, Data.DataSet(), Data.DataSet());
        Pessoa b = new Aluno("03-09-2016");
        System.out.println(b);

        /*
        System.out.println("Valor da bolsa: ");
        int bolsa = sc.nextInt();
        Pessoa c = new Bolseiro(nome, cc, Data.DataSet(), Data.DataSet(), bolsa);
        */
        Pessoa c = new Bolseiro("20-08-2016", 3000);
        System.out.println(c);
        sc.close();
    }
}
