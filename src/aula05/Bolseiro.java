package aula05;

public class Bolseiro extends Aluno {
    private int bolsa;
    public int getBolsa() {return bolsa;}
    public void setBolsa(int bolsa) {this.bolsa = bolsa;}

    public Bolseiro(String datainscr, int bolsa) {
        super(datainscr);
        this.bolsa = bolsa;
    }
    public Bolseiro(String nome, int cc, String data, String datainscr, int bolsa) {
        super(nome, cc, data, datainscr);
        this.bolsa = bolsa;
    }

    @Override
    public String toString() {
        return "Bolseiro{" + super.toString() + ", bolsa= " + bolsa + "€" + '}';
    }
}
