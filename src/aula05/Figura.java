package aula05;
import java.util.*;

public class Figura {
    Scanner sc = new Scanner(System.in);
    private int x;
    private int y;

    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }

    public Figura(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Figura() {
        System.out.print("x = ");
        x = sc.nextInt();
        System.out.print("y = ");
        y = sc.nextInt();
    }

    @Override
    public String toString() {
        return " Figura{" + "Centro(" + x + "," + y + ")" + '}';
    }
}
