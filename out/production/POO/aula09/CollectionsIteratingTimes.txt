arraylist:
add    - 1.81   (ms)
search - 32.11  (ms)
remove - 5.94   (ms)

linkedlist:
add    - 1.52
search - 48.90
remove - 31.25

stack:
add    - 2.70
search - 47.04
remove - 98.42

treeset:
add    - 14.83
search - 6.13
remove - 9.95