# README #

This is a simple repository made by Filipe Pires for his first year of college.

### What is this repository for? ###

* Saving all exercises proposed in class
* Managing the differences from version to version

### How do I get set up? ###

* read the comments of each commit
* if you have any doubt contact the creator through email

### Who do I talk to? ###

* Repo owner / admin - Filipe Pires